//var express = require('express');
var request = require('supertest');
var assert = require('assert');
var host = 'http://localhost';
var user = {
  username: 'test@gmail.com',
  password: 'test',
};
describe('Ma Plot Test Suite', function() {
  var agent = request.agent(host);
  it('login ' + user.username + ' + 으로 로그인하여야 한다.', function(done) {
    agent.post('/login')
      .send(user)
      .expect(302) //Moved Temporarily
      .expect('Location', '/')
      .end(function(err, res) {
        if (err) return done(err); // 이부분이 빠지만, 위 expect 에러를 확인하지 못한다.
        done();
      });
  });
  it('path가 존재하여야 한다.', function(done) {
    agent
      .get('/rest/maplot')
      .expect(200, done);
  });
  it('Content Type이 application/json 여야한다.', function(done) {
    agent
      .get('/rest/maplot')
      .expect('content-type', /json/, done);
  });
  it('JSON Data Format Check', function(done) {
    agent
      .get('/rest/maplot')
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        assert.equal('OK', transfer_object.message);
        assert.equal(20429, transfer_object.data.plot_list.length);
        assert.equal('DNAJC6|9829', transfer_object.data.plot_list[0].title);
        done();
      });
  });
});
