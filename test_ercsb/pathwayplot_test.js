//var express = require('express');
var request = require('supertest');
var assert = require('assert');
var host = 'http://localhost';
var user = {
  username: 'test@gmail.com',
  password: 'test',
};
describe('Pathway Plot Test Suite', function() {
  var agent = request.agent(host);
  it('login ' + user.username + ' + 으로 로그인하여야 한다.', function(done) {
    agent.post('/login')
      .send(user)
      .expect(302) //Moved Temporarily
      .expect('Location', '/')
      .end(function(err, res) {
        if (err) return done(err); // 이부분이 빠지만, 위 expect 에러를 확인하지 못한다.
        done();
      });
  });

  it('path가 존재하여야 한다.', function(done) {
    agent
      .get('/rest/pathwayplot')
      .expect(200, done);
  });
  it('parameter가 없으면 transfer_object error code 1000', function(done) {
    agent
      .get('/rest/pathwayplot')
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        //console.log(transfer_object.data.graph);
        assert.equal(1000, transfer_object.status);
        assert.equal('No parameter', transfer_object.message);
        done();
      });
  });
  it('Content Type이 application/json 여야한다.', function(done) {
    agent
      .get('/rest/pathwayplot?cancer_type=luad&sample_id=Pat1099&seq=1&filter=1:')
      .expect('content-type', /json/, done);
  });
  it('JSON Data Format Check', function(done) {
    agent
      .get('/rest/pathwayplot?cancer_type=luad&sample_id=Pat1099&seq=1&filter=1:')
      //.field('gene','EGFR')
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        //console.log(transfer_object.data.graph);
        assert.equal('OK', transfer_object.message);
        assert.equal('luad', transfer_object.data.cancer_type);
        assert.equal(44, transfer_object.data.pathway_list.length);
        assert.equal(0, transfer_object.data.gene_list.length);
        done();
      });
  });
});
