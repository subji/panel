var request = require('supertest');
var assert = require('assert');
var host = 'http://localhost';
var getConnection = require('../routes/modules/mysql_connection');

var clinical = require('./test_data.json').clinical;
var user = require('./test_data.json').user;

var cohort = 'GDAC';
var cancer_type = 'luad';
var analysis_id = 'SMCLUAD1609050115';

describe('서비스테스트', function() {
  var agent = request.agent(host);
  it('login ' + user.username + ' + 으로 로그인하여야 한다.', function(done) {
    agent.post('/login')
      .send(user)
      .expect(302) //Moved Temporarily
      .expect('Location', '/menu/')
      .end(function(err, res) {
        if (err) return done(err); // 이부분이 빠지만, 위 expect 에러를 확인하지 못한다.
        done();
      });
  });

  it('Analysis > should redirect to summary', function(done) {
    agent.get('/menu/analysis/first?cancer_type=' + cancer_type + '&analysis_id=' + analysis_id)
      .expect(302)
      .expect('Location', 'summary')
      .expect('Content-Type', /text\/plain/)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('Analysis > Analysis List', function(done) {
    agent.get('/models/analysis/list')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        assert.equal(true, Array.isArray(data));
        assert.equal(true, data.length > 1);
        done();
      });
  });

  it('Analysis > Clinical data : DB에서 Upload할 Clinical data 있으면 삭제한다.', function(done) {
    getConnection(function(connection) {
      connection.query('delete from CGIS.GS_CLINICAL_LUAD_TB where analysis_id = ?', [clinical.analysis_id], function(err, rows, fields) {
        if (err) assert(false, err.code); //throw err;
        assert.equal(1, rows.affectedRows);
        done();
      });
    });
  });

  it('Analysis > Clinical data : Upload', function(done) {
    agent.post('/models/analysis/clinical')
      .send({
        'cancer_type': 'luad',
        'row': JSON.stringify(clinical)
      })
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        assert.equal('success', data.type);
        done();
      });
  });

  it('Analysis > Clinical data : 수정', function(done) {
    agent.put('/models/analysis/clinical')
      .send({
        'cancer_type': 'luad',
        'pk': clinical.analysis_id,
        'name': 'lost_follow_up',
        'value': 'No'
      })
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        assert.equal('Updated!', data.message);
        done();
      });
  });

  it('Analysis > Summary', function(done) {
    agent.get('/menu/analysis/summary')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'analysis_id': analysis_id
      })
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('Analysis > Variants', function(done) {
    agent.get('/models/analysis/getAnalysisVariantList')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'analysis_id': analysis_id,
        'frequency': '0',
        'classification': 'All',
        'cosmic': 'Y',
        'filter_option': ':',
        'driver': 'Y',
      })
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        assert.equal(true, Array.isArray(data));
        assert.equal(2, data.length);
        done();
      });
  });

  it('Analysis > Cohort Setting : 화면 생성', function(done) {
    agent.get('/menu/analysis/globalsetting')
      .query({
        'source': cohort
      })
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        // 생성된 HTML을 테스트할 방법이 딱히 없다. 시작이 다음과 같음을 테스트함.
        assert.ok(res.text.startsWith('<div id="accordion"'));
        done();
      });
  });

  it('Analysis > Cohort Setting : Total Cohort Selection', function(done) {
    agent.get('/models/cohort/')
      .query({
        'source': cohort,
        'cancer_type': cancer_type
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        // console.log(data);
        assert.equal(522, data);
        done();
      });
  });

  it('Analysis > Cohort Setting : Cohort Selection With Filter', function(done) {
    agent.get('/models/cohort/filter')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'filter_option': '1:'
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        // console.log(data);
        assert.equal(522, data.total);
        assert.equal(334, data.cnt);
        done();
      });
  });

  it('Analysis > Variants : JSON Data Check', function(done) {
    // agent.get('/rest/needleplot?cancer_type=' + cancer_type + '&analysis_id=' + analysis_id + '&gene=ABL1&transcript=ENST00000318560&classification=All&filter=')
    agent.get('/rest/needleplot')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'analysis_id': analysis_id,
        'gene': 'ABL1',
        'transcript': 'ENST00000318560',
        'classification': 'All',
        'filter': ':',
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        // console.log(transfer_object.data.graph);
        assert.equal('OK', transfer_object.message);
        assert.equal('ABL1', transfer_object.data.name);
        assert.equal(15, transfer_object.data.public_list.length);
        assert.equal(0, transfer_object.data.patient_list.length);
        assert.equal(4, transfer_object.data.graph.length);
        done();
      });
  });

  it('Analysis > Pathways : JSON Data Check', function(done) {
    agent.get('/rest/pathwayplot')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'analysis_id': analysis_id,
        'filter': ':',
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        //console.log(transfer_object.data.graph);
        assert.equal('OK', transfer_object.message);
        assert.equal('luad', transfer_object.data.cancer_type);
        assert.equal(44, transfer_object.data.pathway_list.length);
        assert.equal(2, transfer_object.data.gene_list.length);
        done();
      });
  });

  it('Analysis > Theraphies : JSON Data Check', function(done) {
    agent.get('/models/drug/getDrugListByPatient')
      .query({
        'cancer_type': cancer_type,
        'analysis_id': analysis_id,
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        assert.equal(true, Array.isArray(data.drugs));
        assert.equal(57, data.drugs.length);
        assert.equal(2,data.geneset.split(',').length);
        done();
      });
  });

  it('Analysis > Theraphies : JSON Data Check', function(done) {
    agent.get('/models/drug/getDrugListByCancer')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'analysis_id': analysis_id,
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var data = res.body;
        assert.equal(true, Array.isArray(data));
        assert.equal(243, data.length);
        done();
      });
  });

  // it('Analysis > Expressions : JSON Data Check', function(done) {
  //   agent.get('/rest/expressions')
  //     .query({
  //       'source': cohort,
  //       'cancer_type': cancer_type,
  //       'analysis_id': analysis_id,
  //       'signature': 'SMCLUAD - NMF Subtype 1',
  //       'filter': ':',
  //     })
  //     .expect(200)
  //     .expect('content-type', /json/)
  //     .end(function(err, res) {
  //       if (err) return done(err);
  //       var transfer_object = res.body;
  //       //console.log(transfer_object.data.graph);
  //       assert.equal('OK', transfer_object.message);
  //       assert.equal(107, transfer_object.data.subtype_list.length);
  //       assert.equal(522, transfer_object.data.patient_list.length);
  //       assert.equal(4, transfer_object.data.signature_list.length);
  //       assert.equal(69, transfer_object.data.gene_list.length);
  //       assert.equal(31415, transfer_object.data.cohort_rna_list.length);
  //       assert.equal(0, transfer_object.data.sample_rna_list.length);
  //       done();
  //     });
  // });

  it('Analysis > Comutation : JSON Data Check', function(done) {
    agent.get('/rest/comutationplot')
      .query({
        'source': cohort,
        'cancer_type': cancer_type,
        'analysis_id': analysis_id,
        'filter': ':',
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        //console.log(transfer_object.data.graph);
        assert.equal('OK', transfer_object.message);
        assert.equal(2469, transfer_object.data.mutation_list.length);
        assert.equal(40, transfer_object.data.gene_list.length);
        assert.equal(2, transfer_object.data.patient_list.length);
        done();
      });
  });

  it('Analysis > Mutual Exclusivity : JSON Data Check', function(done) {
    agent.get('/rest/mutext')
      .query({
        'source': cohort,
        'cancer_type':cancer_type
      })
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        var transfer_object = res.body;
        assert.equal('OK', transfer_object.message);
        assert.equal(522, transfer_object.data.patient_list.length);
        assert.equal(2, transfer_object.data.file_list.length);
        assert.equal(2548, transfer_object.data.variant_types.length);
        done();
      });
  });
});
