//var express = require('express');
var request = require('supertest');
var assert = require('assert');
var getConnection = require('../routes/modules/mysql_connection');
var user = require('./test_data.json').user;
var host = 'http://localhost';

describe('Profile Test Suite', function() {
  var agent = request.agent(host);
  it('login ' + user.username + ' + 으로 로그인하여야 한다.', function(done) {
    agent.post('/login')
      .send(user)
      .expect(302) //Moved Temporarily
      .end(function(err, res) {
        if (err) return done(err);
        assert.equal(res.header.location, '/menu/');
        done();
      });
  });

  it('로그인을 했으면 /models/users/profile 화면 패스(get)가 존재하여야 한다.', function(done) {
    agent.get('/models/users/profile')
      .expect(200, done);
  });

  it('DB에서 테스트할 사용자 ID를 읽어온다.', function(done) {
    getConnection(function(connection) {
      connection.query('select id from CGIS.CM_USERS_TB where username = ?', [user.username], function(err, rows) {
        if (err) assert(false, err.code); //throw err;
        user.id = rows[0].id;
        assert.equal('number', typeof user.id);
        done();
      });
    });
  });

  var new_fullname = '테스트' + Math.random(); //임시 이름을 하나 만든다.
  var data = {
    name: 'fullname',
    value: new_fullname
  };
  it('사용자 정보 중 사용자명이 수정되어야한다.', function(done) {
    data.pk = user.id;
    agent.put('/models/users')
      .send(data)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('수정된 사용자 이름이 DB에 정확히 반영되어 있어야한다.', function(done) {
    getConnection(function(connection) {
      connection.query('select fullname from CGIS.CM_USERS_TB where username = ?', [user.username], function(err, rows, fields) {
        if (err) assert(false, err.code); //throw err;
        assert.equal(new_fullname, rows[0].fullname);
        done();
      });
    });
  });

  it('사용자명을 원래대로 수정하는 요청(Put, /models/users)이 제대로 작동하여야한다.', function(done) {
    data.value = user.fullname;
    agent.put('/models/users')
      .send(data)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  var new_password = 'new_password';
  it('패스워드를 수정하는 요청(Put, /models/users)이 제대로 작동하여야한다.', function(done) {
    data.name = 'p';
    data.value = new_password;
    agent.put('/models/users')
      .send(data)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('패스워드를 원래대로 수정하는 요청(Put, /models/users)이 제대로 작동하여야한다.', function(done) {
    data.value = user.password;
    agent.put('/models/users')
      .send(data)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });
});
