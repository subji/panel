var express = require('express');
var walk = require('walk');
var util = require('./modules/util');
var getConnection = require('./modules/mysql_connection');
var plot = require('./modules/plot');
var to = require('./modules/transfer_object');
var router = express.Router();

router.get('/comutationplot', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.query.cancer_type;
	var analysis_id = req.query.analysis_id;
	var filter = req.query.filter;

	// if (cancer_type === undefined || sample_id === undefined || filter === undefined) {
	if (util.isUndefined(source, cancer_type, analysis_id, filter))
		return res.json(to.noparameter());

	var transfer_object = to.default();
	transfer_object.data = {
		id: 'id',
		name: 'Co Mutation Plot with TCGA',
		type: cancer_type.toUpperCase(),
		mutation_list: [],
		gene_list: [],
		group_list: [],
		patient_list: [],
	};
	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getComutationplotMutationList(?,?,?)', [source, cancer_type, filter], function(mut_err, mut_rows) {
			if (mut_err) return next(mut_err);
			transfer_object.data.mutation_list = mut_rows[0];

			connection.query('CALL CGIS.sp_getComutationplotMutationGeneList(?)', [cancer_type], function(sig_err, sig_rows) {
				if (sig_err) return next(sig_err);
				transfer_object.data.gene_list = sig_rows[0];
				connection.query('CALL CGIS.sp_getComutationplotMutationGroupList(?,?,?)', [source, cancer_type, filter], function(group_err, group_rows) {
					if (group_err) return next(group_err);

					var groupList = [];
					var keys = Object.keys(group_rows[0][0]);
					for (var i = 1; i < keys.length; i++) { // 0번은 sample_id 이므로 skip
						//console.log(key);
						groupList.push({
							name: keys[i],
							data: []
						});
					}
					group_rows[0].forEach(function(_data) {
						for (var i = 0; i < groupList.length; i++) {
							groupList[i].data.push({
								participant_id: _data.participant_id,
								value: _data[groupList[i].name]
							});
						}
					});
					transfer_object.data.group_list = groupList;

					connection.query('CALL CGIS.sp_getComutationplotMutationPatientList(?,?)', [cancer_type, analysis_id], function(patient_err, patient_rows) {
						if (patient_err) return next(patient_err);

						var patient_list = [];

						// transfer_object.data.patient_list = patient_rows[0];
						patient_rows[0].forEach(function(data) {
							patient_list.push({
								'participant_id': data.analysis,
								'gene': data.gene,
								'type': data.type,
							});
						});

						transfer_object.data.patient_list = patient_list;

						res.json(transfer_object);
					});
				});
			});
		});
	});
});

router.get('/needleplot', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.query.cancer_type;
	var analysis_id = req.query.analysis_id;
	var gene = req.query.gene;
	var transcript = req.query.transcript;
	var classification = req.query.classification;
	var filter = req.query.filter;

	var transfer_object = to.default();

	if (util.isUndefined(source, cancer_type, analysis_id, gene, transcript, classification, filter))
		return res.json(to.noparameter());

	transfer_object.data = {
		name: gene,
		title: cancer_type.toUpperCase() + ': ' + gene + ',' + transcript,
		x_axis_title: 'aa',
		y_axis_title: '# Mutations',
		public_list: [],
		patient_list: [],
		graph: {},
	};

	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getNeedleplotPublicList(?,?,?,?,?,?)', [source, cancer_type, gene, transcript, classification, filter], function(err, rows) {
			if (err) return next(err);
			transfer_object.data.public_list = rows[0];
			connection.query('CALL CGIS.sp_getNeedleplotPatientList(?,?,?,?,?)', [cancer_type, analysis_id, gene, transcript, classification], function(p_err, p_rows) {
				if (p_err) return next(p_err);
				transfer_object.data.patient_list = p_rows[0];
				connection.query('CALL CGIS.sp_getNeedleplotGraph(?)', [gene], function(g_err, g_rows) {
					if (g_err) return next(g_err);
					transfer_object.data.graph = g_rows[0];
					res.json(transfer_object);
				});
			});
		});
	});

});

router.get('/pathwayplot', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.query.cancer_type;
	var analysis_id = req.query.analysis_id;
	// var seq = req.query.seq;
	var filter = req.query.filter;

	var transfer_object = to.default();

	// if (cancer_type === undefined || sample_id === undefined || seq === undefined || filter === undefined) {
	if (util.isUndefined(source, cancer_type, analysis_id, filter))
		return res.json(to.noparameter());

	transfer_object.data = {
		cancer_type: cancer_type,
		// seq: seq,
		pathway_list: [],
		gene_list: []
	};


	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getPathwayplot(?,?,?)', [source, cancer_type, filter], function(err, rows) {
			if (err) return next(err);
			transfer_object.data.pathway_list = rows[0];
			transfer_object.data.drug_list = rows[1];
			// res.json(transfer_object);
			connection.query('CALL CGIS.sp_getPathwayplotGeneList(?,?,?)', [source, cancer_type, analysis_id], function(err, rows) {
				if (err) return next(err);
				transfer_object.data.gene_list = rows[0].map(function(_d) {
					return _d.gene_id;
				});
				res.json(transfer_object);
			});
		});
	});
});

var sort_func = function(a, b) {
	return (a < b) ? -1 : 1;
};

/*
 * x축 start, end .y축 start, end를 구하는 함수.
 * 리스트를 소팅하여 시작과 끝 값을 min, max로 구한다.
 */
var getMaxMin = function(plot_list) {
	var xlist = [];
	var ylist = [];

	plot_list.map(function(_d, _i) {
		xlist[_i] = Number(_d.x);
		ylist[_i] = Number(_d.y);
	});
	xlist.sort(sort_func);
	ylist.sort(sort_func);
	return {
		x: {
			min: xlist[0],
			max: xlist[xlist.length - 1]
		},
		y: {
			min: ylist[0],
			max: ylist[ylist.length - 1]
		}
	};
};

// router.get('/expressions', function(req, res, next) {
// 	var source = req.query.source;
// 	var cancer_type = req.query.cancer_type;
// 	var signature = req.query.signature || 'PAM50';
// 	var sample_id = req.query.sample_id;
// 	var filter = req.query.filter;
//
// 	var transfer_object = to.default();
//
// 	if (util.isUndefined(source, cancer_type, signature, sample_id, filter))
// 		return res.json(to.noparameter());
//
// 	getConnection(function(connection) {
// 		connection.query('Call CGIS.sp_getExpressions(?,?,?,?,?)', [source, cancer_type, signature, sample_id, filter], function(err, rows) {
// 			if (err) return next(err);
//
// 			transfer_object.data = {
// 				subtype_list: rows[0],
// 				patient_list: rows[1],
// 				signature_list: rows[2],
// 				gene_list: rows[3],
// 				cohort_rna_list: rows[4],
// 				sample_rna_list: rows[5]
// 			};
// 			res.json(transfer_object);
// 		});
// 	});
// });

router.get('/mutext', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.query.cancer_type;
	var analysis_id = req.query.analysis_id;

	var transfer_object = to.default();

	if (util.isUndefined(source))
		return res.json(to.noparameter());

	getConnection(function(connection) {
		connection.query('call CGIS.sp_getMutualExclusivity(?,?,?)', [source, cancer_type, analysis_id], function(err, rows) {
			if (err) return next(err);
			console.log(rows[0].length, rows[1].length, rows[2].length);
			transfer_object.data = { patient_list: rows[0], file_list: rows[1], variant_types: rows[2], sample_variants: rows[3] };
			res.json(transfer_object);
		});
	});
});
/*
router.get('/mutext_backup', function(req, res, next) {
  var source = req.query.source;

  var transfer_object = to.default();

  if (util.isUndefined(source))
    return res.json(to.noparameter());

  getConnection(function(connection) {
    connection.query('select filename, contents from BIOBANK.PD_MUTUAL_EXCLUSIVITY_TB where source = ?', [source], function(err, rows) {
      if (err) return next(err);

      transfer_object.data = { mutext: rows };
      res.json(transfer_object);
    });
  });
});
*/

router.get('/panel', function(req, res, next) {

	getConnection(function(connection) {
		connection.query('select panel_id,name,version from CGIS.CM_PANEL_TB', [], function(err, rows) {
			if (err) return next(err);

			res.json(rows);
		});
	});
});

var FOLDER = '/data/public/ftpuser';
router.get('/user_files', function(req, res, next) {
	var ext = req.query.ext;
	var filter = req.query.filter;
	var search = req.query.q || '';
	var files = [];

	// Walker options
	var walker = walk.walk(FOLDER, { followLinks: false });

	console.log(ext, search, filter);
	walker.on('file', function(root, stat, next) {
		// Add this file to the list of files
		var dir = root.substring(FOLDER.length);
		if (stat.name.toLowerCase().endsWith(ext)) {
			files.push({ id: root +'/'+ stat.name, text: '.' + dir + '/' + stat.name, filename: stat.name });
		}
		next();
	});

	walker.on('end', function() {
		if (filter != '')
			files = files.filter(function(file) {
				return file.filename.indexOf(filter) > 0;
			});
		if (search != '')
			files = files.filter(function(file) {
				return file.filename.indexOf(search) > 0;
			});
		res.json({ results: files });
	});

});

module.exports = router;
