var util = {};

util.isUndefined = function() {
  for (var i = 0; i < arguments.length; i++) {
    if (typeof arguments[i] === 'undefined') return true;
  }
  return false;
};

module.exports = util;
