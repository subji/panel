var message = {};

message.insertSuccess = function(id, txt) {
	var msg = txt || '입력하였습니다.';
	return { success: true, id: id, message: msg };
};

message.insertFail = function(txt) {
	var msg = txt || '문제가 발생하여 입력하지 못했습니다.';
	return { success: false, message: msg };
};

message.updateSuccess = function(txt) {
	var msg = txt || '수정하였습니다.';
	return { success: true, message: msg };
};

message.updateFail = function(txt) {
	var msg = txt || '문제가 발생하여 수정하지 못했습니다.';
	return { success: false, message: msg };
};

message.deleteSuccess = function(txt) {
	var msg = txt || '삭제하였습니다.';
	return { success: true, message: msg };
};

message.deleteFail = function(txt) {
	var msg = txt || '문제가 발생하여 삭제하지 못했습니다.';
	return { success: false, message: msg };
};

message.paramError = function(txt) {
	var msg = txt || '필수항목이 누락되었습니다.';
	return { success: false, message: msg };
};

message.dbError = function(err) {
	if (err) console.log('DB Error', err); //Debug

	var msg = err.message;
	if(err.code === 'ER_DUP_ENTRY')
		msg = '동일 항목이 이미  존재합니다.'

	return { success: false, message: msg };
};

module.exports = message;
