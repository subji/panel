var mysql = require('mysql');
var biobank = require('../../config.json').biobank_db;

//var connection = mysql.createConnection({
// connection 개수는 cluster 개수가 max이다.
var pool = mysql.createPool({
    supportBigNumbers: true,
    connectionLimit: 100,
    // waitForConnections: false,
    host: biobank.host,
    database: biobank.database,
    user: biobank.user,
    password: biobank.password
});
var count = 0;

pool.on('connection', function(connection) {
    console.log('%dth connection is created!!!',++count);
});
pool.on('enqueue', function() {
    console.log('Waiting for available connection slot');
});

var getConnection = function(callback) {
    pool.getConnection(function(err, connection) {
        if (err) {
            console.error('error connecting: ' + err.stack);
            return err;
        }
        //console.log('connected as id ' + connection.threadId);
        callback(connection);
        connection.release();
    });
};
module.exports = getConnection;
