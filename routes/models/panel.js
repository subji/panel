var express = require('express');
var multiline = require('multiline');
var message = require('../modules/message');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var router = express.Router();
// CM_PANEL_TB 의 값을 내려받는다.
router.get('/list', function(req, res, next) {
	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getPanelList()', function(err, rows) {
			if (err) return next(err);
			res.json(rows[0]);
		});
	});
});

// Insert
var INSERT_SQL = multiline(function() {
	/*
	insert into CGIS.CM_PANEL_TB
	 (panel_id, name, version, hugo_symbols)
	values
	 (?,?,?,?)
	*/
});
// Insert 는 "POST"
router.post('/', function(req, res, next) {
	console.log(req.body);
	var hugo_symbols = req.body.hugo_symbols;
	if (hugo_symbols)
		hugo_symbols = hugo_symbols.split(',').sort().join(',');
	// 위에 multiline 으로 정의된 SQL 문을 사용해서 새로운 PANEL 행을 넣는다.
	getConnection(function(connection) {
		connection.query(INSERT_SQL, [
			req.body.panel_id,
			req.body.name,
			req.body.version,
			hugo_symbols
		], function(err, row) {
			if (err)
				return res.json(message.dbError(err));

			if (row.affectedRows === 1)
				res.json(message.insertSuccess(row.insertId));
			else
				res.json(message.insertFail());
		});
	});
});

// Update
var UPDATE_SQL = multiline(function() {
	/*
	update CGIS.CM_PANEL_TB
			set panel_id = ?,
			name = ?,
			version = ?,
			hugo_symbols = ?
	 where id = ?
	*/
});
// Update 는 "PUT"
router.put('/', function(req, res, next) {
	var id = req.body.id;
	var panel_id = req.body.panel_id;
	var name = req.body.name;
	var version = req.body.version;
	var hugo_symbols = req.body.hugo_symbols;
	if (hugo_symbols)
		hugo_symbols = hugo_symbols.split(',').sort().join(',');
	// console.log(hugo_symbols);

	// Check Primary Key
	if (util.isUndefined(id, panel_id, name, version, hugo_symbols))
		return res.json(message.paramError());
	// 위에 정의된 UPDATE 문을 사용해서 해당 id 의 행을 업데이트 한다.
	getConnection(function(connection) {
		connection.query(UPDATE_SQL, [panel_id, name, version, hugo_symbols, id], function(err, rows) {
			if (err)
				return res.json(message.dbError(err));
			else
				return res.json(message.updateSuccess());
		});
	});
});

// Delete
var DELETE_SQL = multiline(function() {
	/*
	delete from CGIS.CM_PANEL_TB
	 where id = ?
	*/
});
// Delete 는 "DELETE" 요청.
router.delete('/', function(req, res, next) {
	var id = req.body.id;
	
	// Check Primary Key
	if (util.isUndefined(id))
		return res.json(message.paramError());
	// 위에 정의된 DELETE SQL 을 사용해서 해당 행을 삭제한다.
	getConnection(function(connection) {
		connection.query(DELETE_SQL, [id], function(err, rows) {
			if (err)
				return res.json(message.dbError(err));
			else
				return res.json(message.deleteSuccess());
		});
	});

});

module.exports = router;
