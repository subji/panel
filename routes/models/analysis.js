var express = require('express');
var config = require('../../config.json');
var util = require('../modules/util');
var message = require('../modules/message');
var getConnection = require('../modules/mysql_connection');
// var https = require("https");
// var querystring = require('querystring');
var exec = require('child_process').exec;
var multer = require('multer');
var async = require('async');
var fs = require('fs');
var path = require("path");
var router = express.Router();
// Register and Analysis 버튼을 클릭하면 아래의 스크립트를 실행한다.
var panel_script = '/data/pipeline/panel/bin/run.sh';

// router.get('/list_cgis', function(req, res, next) {
// 	getConnection(function(connection) {
// 		connection.query('CALL CGIS.sp_getSampleList()', function(err, rows) {
// 			if (err) return next(err);
// 			res.json(rows[0]);
// 		});
// 	});
// });
// Cancer Panel Analysis 화면의 리스트를 보여주기 위한 화면.
router.get('/list', function(req, res, next) {
	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getAnalysisList()', function(err, rows) {
			if (err) return next(err);
			res.json(rows[0]);
		});
	});
});

var ifnull = function(data, value) {
	return (data === undefined) ? value : data;
};

router.get('/', function(req, res, next) {
	var analysis_id = req.query.analysis_id;

	if (util.isUndefined(analysis_id))
		return res.status(400).send('Not sufficient paramerters');

	getConnection(function(connection) {
		connection.query('CALL sp_getAnalysis(?)', [analysis_id], function(err, rows) {
			if (err)
				return next(err);
			res.render('menu/sample/analysis', {
				analysis: rows[0][0],
				analysisHistory: rows[1],
				clinical: rows[2][0],
				categories: rows[3],
			});
		});
	});
});
// 새로운 PANEL 정보를 입력 하는 코드.
router.post('/', function(req, res, next) {
	var analysis = req.body;
	var user = req.user;
	console.log(analysis, user);
	if (util.isUndefined(analysis, user))
		return res.status(400).send('Not sufficient paramerters');
	var options = [
		user.id,
		analysis.panel_id,
		analysis.version,
		analysis.cancer_type,
		analysis.exist_normal,
		analysis.vcf_location,
		analysis.bam_normal_location,
		analysis.bai_normal_location,
		analysis.bam_tumor_location,
		analysis.bai_tumor_location,
		analysis.clinical_data_ref,
		analysis.sequencing_interface_ref,
		analysis.comments
	];
	console.log('options', options);

	getConnection(function(connection) {
		connection.query('select CGIS.sf_registerAnalysis(?,?,?,?,?,?,?,?,?,?,?,?,?) as analysis_id', options, function(err, rows) {
			if (err)
				return res.json(message.dbError(err));
			analysis.analysis_id = rows[0].analysis_id;

			// 동인씨가 만든 쉘 스크립트에서 파일 복제함.2018.04.16
			// fs.createReadStream(analysis.bam_tumor_location).pipe(fs.createWriteStream( './public/data/bam/' + analysis.analysis_id + '.bam'));
			// fs.createReadStream(analysis.bai_tumor_location).pipe(fs.createWriteStream( './public/data/bam/' + analysis.analysis_id + '.bam.bai'));
			// 위에서 선언한 패널 스크립트를 실행하기 위한 옵션을 배열에 저장한다.
			var cmdArray = []
			cmdArray.push(panel_script);

			cmdArray.push(analysis.vcf_location);
			cmdArray.push(analysis.bam_tumor_location);
			cmdArray.push(analysis.analysis_id);
			cmdArray.push("'" + user.fullname + "'");
			cmdArray.push(analysis.cancer_type);

			// 위에서 배열에 저장된 데이터를 ' ' 로 합친 문자열로 만들고
			// exec() 함수를 사용해 표준 입력 후 표준 출력을 받는다.
			var cmdLine = cmdArray.join(' ');
			console.log(cmdLine);
			// 아래 코드로 위에 쉘스크립트를 실행한다. (패널 분석 및 등록)
			exec(cmdLine, function(error, stdout, stderr) {
				if (error) return console.log('error', cmdLine, error);

				console.log('stdout', cmdLine, stdout);
				console.log('stderr', stderr);
				// res.json(message.insertSuccess(analysis.analysis_id));
			});
			// 입력이 완료되었다는 것을 메세지로 알림.
			res.json(message.insertSuccess(analysis.analysis_id));
		});
	});
});
// Variant 화면에 보여질 데이터를 가져오는 코드.
router.get('/getAnalysisVariantList', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.query.cancer_type;
	var analysis_id = req.query.analysis_id;
	var frequency = req.query.frequency;
	var classification = req.query.classification;
	var cosmic = req.query.cosmic;
	var filter_option = req.query.filter_option;

	if (util.isUndefined(source, cancer_type, analysis_id, frequency, classification, cosmic, filter_option))
		return res.status(400).send('Not sufficient paramerters');

	getConnection(function(connection) {
		var params = [source, cancer_type, analysis_id, classification, cosmic, frequency, filter_option];
		connection.query('CALL CGIS.sp_getAnalysisVaiants(?,?,?,?,?,?,?)', params, function(err, rows) {
			if (err) return next(err);
			items = rows[0];
			// Mining Drugs
			async.each(items, function(item, cb) {
				connection.query('CALL CGIS.sp_getMiningDrugList(?,?,?,?)', [item.cancer_type, item.gene, item.alt, item.cds], function(err, rows) {
					if (err) cb(err);
					item.miningdrugs = rows[0];
					// OncoKB Drugs
					connection.query('CALL CGIS.sp_getOncokbDrugList(?,?)', [item.gene, item.alt], function(err, rows) {
						if (err) cb(err);
						item.oncokbdrugs = rows[0];
						connection.query('CALL CGIS.sp_getFDAApprovedDrugForGene(?,?)', [item.cancer_type, item.gene], function(err, rows) {
							if (err) cb(err);
							item.fda_in_gene_drugs = rows[0];
							connection.query('CALL CGIS.sp_getFDAApprovedDrugForAlt(?,?,?,?)', [item.cancer_type, item.gene, item.alt, item.cds], function(err, rows) {
								if (err) cb(err);
								item.fda_in_alt_drugs = rows[0];
								cb();
							});
						});
					});
				});
			}, function(err) {
				if (err) return next(err);
				res.json(items);
			});
		});
	});
});
// 화면에서 Clinical Edit 버튼을 누르면 
router.put('/clinical', function(req, res, next) {
	var cancer_type = req.body.cancer_type;
	var pk = req.body.pk;
	var colname = req.body.name;
	var colvalue = req.body.value;

	// Check Primary Key & cancer_type
	if (util.isUndefined(cancer_type, pk, colname, colvalue))
		return res.status(400).send('ERRPK:Bad Request!!!');

	getConnection(function(connection) {
		var table_name = 'GS_CLINICAL_' + cancer_type.toUpperCase() + '_TB';

		connection.query('update ?? set ?? = ? where analysis_id = ?', [table_name, colname, colvalue, pk], function(err, rows) {
			// Check SQL Statement
			if (err) {
				console.log(err);
				return res.status(500).send(err.code);
			}

			// Check whether affected or not
			// console.log(rows);
			if (rows.changedRows != 1) {
				// console.log(rows);
				return res.status(500).send(rows.changedRows + ' rows affected.');
			}

			// Finally Return
			res.json({
				message: 'Updated!'
			});
		});
	});
});

/**
 * Clinical Data를 업로드한다.
 */
router.post('/clinical', function(req, res, next) {
	var data = req.body;
	var cancer_type = data.cancer_type;
	var row = JSON.parse(data.row);
	if (util.isUndefined(cancer_type, row))
		return res.status(400).send('ERRPK:Bad Request!!!');

	getConnection(function(connection) {
		var table_name = 'GS_CLINICAL_' + cancer_type.toUpperCase() + '_TB';
		connection.query('insert into ?? set ?', [table_name, row], function(err) {
			if (err) res.json({
				type: 'error',
				msg: err.message
			});
			else res.json({
				type: 'success'
			});
		});
	});
});

module.exports = router;
