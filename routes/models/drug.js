var express = require('express');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var router = express.Router();

//http://192.168.191.159/models/drug/getPathwayDrugList?pathway_gene=BRAF&cancer_type=luad
// 전체 목록을 가져가는 방식으로 변환. 2017.12.08
// router.get('/getPathwayDrugList', function(req, res, next) {
//   var cancer_type = req.query.cancer_type;
//   var pathway_gene = req.query.pathway_gene;
//
//   if (util.isUndefined(cancer_type, pathway_gene))
//     return res.status(400).send('Not sufficient paramerters');
//
//   getConnection(function(connection) {
//     connection.query('CALL CGIS.sp_getPathwayplotDrugList(?,?)', [cancer_type, pathway_gene], function(err, rows) {
//       if (err) return next(err);
//       res.json(rows[0]);
//     });
//   });
// });

router.get('/getDrugListByPatient', function(req, res, next) {
  var cancer_type = req.query.cancer_type;
  var analysis_id = req.query.analysis_id;

  if (util.isUndefined(cancer_type, analysis_id))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getDrugListByAnalysisId(?,?)', [cancer_type, analysis_id], function(err, rows) {
      if (err) return next(err);
      res.json({
        geneset: rows[0][0].geneset,
        drugs: rows[1]
      });
    });
  });
});

router.get('/getDrugListByCancer', function(req, res, next) {
  var source = req.query.source;
  var cancer_type = req.query.cancer_type;
  var analysis_id = req.query.analysis_id;

  if (util.isUndefined(source, cancer_type, analysis_id))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getDrugListByCancer(?,?,?)', [source, cancer_type, analysis_id], function(err, rows) {
      if (err) return next(err);
      res.json(rows[0]);
    });
  });
});
// Summary 페이지에서 Driver Gene 을 클릭하였을 때, 모달에 뿌려줄
// 데이터를 요청하여 반환하는 함수.
router.get('/getDriveGeneInfo', function(req, res, next) {
  var gene_id = req.query.gene_id;

  if (util.isUndefined(gene_id))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getDriveGeneInfo(?)', [gene_id], function(err, rows) {
      if (err) return next(err);
      res.json(rows[0]);
    });
  });
});

module.exports = router;
