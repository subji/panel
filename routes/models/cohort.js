var express = require('express');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var router = express.Router();

router.get('/', function(req, res, next) {
  var source = req.query.source;
  var cancer_type = req.query.cancer_type;

  if (util.isUndefined(source, cancer_type))
    return res.status(400).send('Not sufficient paramerters');
  // source(GDAC) 와 해당 암 종에 속한 cohort 의 개수를 계산하는 스토어드펑션을 실행하여 값을 받아온다
  getConnection(function(connection) {
    connection.query('select CGIS.sf_cntOfCohortByCancerType(?,?) cnt', [source, cancer_type], function(err, rows) {
      if (err) return next(err);
      res.json(rows[0].cnt);
    });
  });

});
// GlobalSetting.js 에서 필터된 cohort 를 불러오는 라우터 함수.
router.get('/filter', function(req, res, next) {
  var source = req.query.source;
  var cancer_type = req.query.cancer_type;
  var filter_option = req.query.filter_option;

  if (util.isUndefined(source, cancer_type, filter_option))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getCountOfCohortByFilter(?,?,?)', [source, cancer_type, filter_option], function(err, rows) {
      if (err) return next(err);
      // console.log(rows[0]);
      // 이값을 session에 저장하여 이용할 수 있도록 한다.
      req.session.analysis.source = source;
      req.session.analysis.filter_option = filter_option;

      res.json(rows[0][0]);
    });
  });

});

module.exports = router;
