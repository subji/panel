var express = require('express');
var multiline = require('multiline');
var message = require('../modules/message');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var router = express.Router();

// Insert
// multiline 모듈 사용 /* */ 안의 내용이 multiline 으로 적용된다.
var INSERT_LUAD_SQL = multiline(function() {
	/*
INSERT INTO CGIS.GS_CLINICAL_LUAD_TB
	(analysis_id, vital_status, gender, race, ethnicity, histological_type, anatomic_neoplasm_subdivision, other_dx, history_of_neoadjuvant_treatment, radiation_therapy, pathologic_T, pathologic_N, pathologic_M, pathologic_stage, residual_tumor, egfr_mutation_result, eml4_alk_translocation_result, kras_mutation_result, primary_therapy_outcome_success, followup_treatment_success, tobacco_smoking_history, days_to_death, days_to_last_followup, lost_follow_up, age_at_initial_pathologic_diagnosis, patient_id)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	*/
});

router.post('/luad', function(req, res, next) {
	var data = [
		req.body.analysis_id,
		req.body.vital_status,
		req.body.gender,
		req.body.race,
		req.body.ethnicity,
		req.body.histological_type,
		req.body.anatomic_neoplasm_subdivision,
		req.body.other_dx,
		req.body.history_of_neoadjuvant_treatment,
		req.body.radiation_therapy,
		req.body.pathologic_T,
		req.body.pathologic_N,
		req.body.pathologic_M,
		req.body.pathologic_stage,
		req.body.residual_tumor,
		req.body.egfr_mutation_result,
		req.body.eml4_alk_translocation_result,
		req.body.kras_mutation_result,
		req.body.primary_therapy_outcome_success,
		req.body.followup_treatment_success,
		req.body.tobacco_smoking_history,
		req.body.days_to_death,
		req.body.days_to_last_followup,
		req.body.lost_follow_up,
		req.body.age_at_initial_pathologic_diagnosis,
		req.body.patient_id
	];

	getConnection(function(connection) {
		connection.query(INSERT_LUAD_SQL, data, function(err, row) {
			if (err)
				return res.json(message.dbError(err));

			if (row.affectedRows === 1)
				res.json(message.insertSuccess(row.insertId));
			else
				res.json(message.insertFail());
		});
	});
});

var UPDATE_LUAD_SQL = multiline(function() {
	/*
UPDATE CGIS.GS_CLINICAL_LUAD_TB
	SET
		vital_status = ?,
		gender = ?,
		race = ?,
		ethnicity = ?,
		histological_type = ?,
		anatomic_neoplasm_subdivision = ?,
		other_dx = ?,
		history_of_neoadjuvant_treatment = ?,
		radiation_therapy = ?,
		pathologic_T = ?,
		pathologic_N = ?,
		pathologic_M = ?,
		pathologic_stage = ?,
		residual_tumor = ?,
		egfr_mutation_result = ?,
		eml4_alk_translocation_result = ?,
		kras_mutation_result = ?,
		primary_therapy_outcome_success = ?,
		followup_treatment_success = ?,
		tobacco_smoking_history = ?,
		days_to_death = ?,
		days_to_last_followup = ?,
		lost_follow_up = ?,
		age_at_initial_pathologic_diagnosis = ?,
		patient_id = ?
	WHERE analysis_id = ?
	*/
});

router.put('/luad', function(req, res, next) {
	var data = [
		req.body.vital_status,
		req.body.gender,
		req.body.race,
		req.body.ethnicity,
		req.body.histological_type,
		req.body.anatomic_neoplasm_subdivision,
		req.body.other_dx,
		req.body.history_of_neoadjuvant_treatment,
		req.body.radiation_therapy,
		req.body.pathologic_T,
		req.body.pathologic_N,
		req.body.pathologic_M,
		req.body.pathologic_stage,
		req.body.residual_tumor,
		req.body.egfr_mutation_result,
		req.body.eml4_alk_translocation_result,
		req.body.kras_mutation_result,
		req.body.primary_therapy_outcome_success,
		req.body.followup_treatment_success,
		req.body.tobacco_smoking_history,
		req.body.days_to_death,
		req.body.days_to_last_followup,
		req.body.lost_follow_up,
		req.body.age_at_initial_pathologic_diagnosis,
		req.body.patient_id,
		req.body.analysis_id
	];

	getConnection(function(connection) {
		connection.query(UPDATE_LUAD_SQL, data, function(err, rows) {
			if (err)
				return res.json(message.dbError(err));
			else
				return res.json(message.updateSuccess());
		});
	});
});
// Insert
var INSERT_BRCA_SQL = multiline(function() {
	/*
INSERT INTO CGIS.GS_CLINICAL_BRCA_TB
	(analysis_id, vital_status, race, ethnicity, age_at_initial_pathologic_diagnosis, histological_type, anatomic_neoplasm_subdivision, breast_carcinoma_estrogen_receptor_status, breast_carcinoma_progesterone_receptor_status, lab_proc_her2_neu_immunohistochemistry_receptor_status, her2_immunohistochemistry_level_result, er_level_cell_percentage_category, progesterone_receptor_level_cell_percent_category, other_dx, history_of_neoadjuvant_treatment, hiv_status, radiation_therapy, pathologic_t, pathologic_m, pathologic_n, pathologic_stage, margin_status, lymph_node_examined_count, days_to_death, days_to_last_followup, lost_follow_up, patient_id)
	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	*/
});

router.post('/brca', function(req, res, next) {
	var data = [
		req.body.analysis_id,
		req.body.vital_status ,
		req.body.race ,
		req.body.ethnicity ,
		req.body.age_at_initial_pathologic_diagnosis ,
		req.body.histological_type ,
		req.body.anatomic_neoplasm_subdivision ,
		req.body.breast_carcinoma_estrogen_receptor_status ,
		req.body.breast_carcinoma_progesterone_receptor_status ,
		req.body.lab_proc_her2_neu_immunohistochemistry_receptor_status ,
		req.body.her2_immunohistochemistry_level_result ,
		req.body.er_level_cell_percentage_category ,
		req.body.progesterone_receptor_level_cell_percent_category ,
		req.body.other_dx ,
		req.body.history_of_neoadjuvant_treatment ,
		req.body.hiv_status ,
		req.body.radiation_therapy ,
		req.body.pathologic_t ,
		req.body.pathologic_m ,
		req.body.pathologic_n ,
		req.body.pathologic_stage ,
		req.body.margin_status ,
		req.body.lymph_node_examined_count ,
		req.body.days_to_death ,
		req.body.days_to_last_followup ,
		req.body.lost_follow_up ,
		req.body.patient_id
	];

	getConnection(function(connection) {
		connection.query(INSERT_BRCA_SQL, data, function(err, row) {
			if (err)
				return res.json(message.dbError(err));

			if (row.affectedRows === 1)
				res.json(message.insertSuccess(row.insertId));
			else
				res.json(message.insertFail());
		});
	});
});

var UPDATE_BRCA_SQL = multiline(function() {
	/*
UPDATE GS_CLINICAL_BRCA_TB
	SET
		vital_status = ?,
		race = ?,
		ethnicity = ?,
		age_at_initial_pathologic_diagnosis = ?,
		histological_type = ?,
		anatomic_neoplasm_subdivision = ?,
		breast_carcinoma_estrogen_receptor_status = ?,
		breast_carcinoma_progesterone_receptor_status = ?,
		lab_proc_her2_neu_immunohistochemistry_receptor_status = ?,
		her2_immunohistochemistry_level_result = ?,
		er_level_cell_percentage_category = ?,
		progesterone_receptor_level_cell_percent_category = ?,
		other_dx = ?,
		history_of_neoadjuvant_treatment = ?,
		hiv_status = ?,
		radiation_therapy = ?,
		pathologic_t = ?,
		pathologic_m = ?,
		pathologic_n = ?,
		pathologic_stage = ?,
		margin_status = ?,
		lymph_node_examined_count = ?,
		days_to_death = ?,
		days_to_last_followup = ?,
		lost_follow_up = ?,
		patient_id = ?
	WHERE analysis_id = ?
	*/
});

router.put('/brca', function(req, res, next) {
	var data = [
		req.body.vital_status ,
		req.body.race ,
		req.body.ethnicity ,
		req.body.age_at_initial_pathologic_diagnosis ,
		req.body.histological_type ,
		req.body.anatomic_neoplasm_subdivision ,
		req.body.breast_carcinoma_estrogen_receptor_status ,
		req.body.breast_carcinoma_progesterone_receptor_status ,
		req.body.lab_proc_her2_neu_immunohistochemistry_receptor_status ,
		req.body.her2_immunohistochemistry_level_result ,
		req.body.er_level_cell_percentage_category ,
		req.body.progesterone_receptor_level_cell_percent_category ,
		req.body.other_dx ,
		req.body.history_of_neoadjuvant_treatment ,
		req.body.hiv_status ,
		req.body.radiation_therapy ,
		req.body.pathologic_t ,
		req.body.pathologic_m ,
		req.body.pathologic_n ,
		req.body.pathologic_stage ,
		req.body.margin_status ,
		req.body.lymph_node_examined_count ,
		req.body.days_to_death ,
		req.body.days_to_last_followup ,
		req.body.lost_follow_up ,
		req.body.patient_id,
		req.body.analysis_id
	];

	getConnection(function(connection) {
		connection.query(UPDATE_BRCA_SQL, data, function(err, rows) {
			if (err)
				return res.json(message.dbError(err));
			else
				return res.json(message.updateSuccess());
		});
	});
});

// Insert
var INSERT_GBM_SQL = multiline(function() {
	/*
INSERT INTO CGIS.GS_CLINICAL_GBM_TB
	(analysis_id, vital_status, gender, race, ethnicity, histological_type, anatomic_neoplasm_subdivision, history_of_neoadjuvant_treatment, other_dx, radiation_therapy, primary_therapy_outcome_success, followup_treatment_success, age_at_initial_pathologic_diagnosis, days_to_last_followup, days_to_death, lost_follow_up, patient_id)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	*/
});

router.post('/gbm', function(req, res, next) {
	var data = [
		req.body.analysis_id,
		req.body.vital_status ,
		req.body.gender,
		req.body.race,
		req.body.ethnicity,
		req.body.histological_type,
		req.body.anatomic_neoplasm_subdivision,
		req.body.history_of_neoadjuvant_treatment,
		req.body.other_dx,
		req.body.radiation_therapy,
		req.body.primary_therapy_outcome_success,
		req.body.followup_treatment_success,
		req.body.age_at_initial_pathologic_diagnosis,
		req.body.days_to_last_followup,
		req.body.days_to_death,
		req.body.lost_follow_up,
		req.body.patient_id
	];

	getConnection(function(connection) {
		connection.query(INSERT_GBM_SQL, data, function(err, row) {
			if (err)
				return res.json(message.dbError(err));

			if (row.affectedRows === 1)
				res.json(message.insertSuccess(row.insertId));
			else
				res.json(message.insertFail());
		});
	});
});

var UPDATE_GBM_SQL = multiline(function() {
	/*
UPDATE GS_CLINICAL_GBM_TB
	SET
		vital_status = ?,
		gender = ?,
		race = ?,
		ethnicity = ?,
		histological_type = ?,
		anatomic_neoplasm_subdivision = ?,
		history_of_neoadjuvant_treatment = ?,
		other_dx = ?,
		radiation_therapy = ?,
		primary_therapy_outcome_success = ?,
		followup_treatment_success = ?,
		age_at_initial_pathologic_diagnosis = ?,
		days_to_last_followup = ?,
		days_to_death = ?,
		lost_follow_up = ?,
		patient_id = ?
	WHERE analysis_id = ?
	*/
});

router.put('/gbm', function(req, res, next) {
	var data = [
		req.body.vital_status ,
		req.body.gender,
		req.body.race,
		req.body.ethnicity,
		req.body.histological_type,
		req.body.anatomic_neoplasm_subdivision,
		req.body.history_of_neoadjuvant_treatment,
		req.body.other_dx,
		req.body.radiation_therapy,
		req.body.primary_therapy_outcome_success,
		req.body.followup_treatment_success,
		req.body.age_at_initial_pathologic_diagnosis,
		req.body.days_to_last_followup,
		req.body.days_to_death,
		req.body.lost_follow_up,
		req.body.patient_id,
		req.body.analysis_id
	];

	getConnection(function(connection) {
		connection.query(UPDATE_GBM_SQL, data, function(err, rows) {
			if (err)
				return res.json(message.dbError(err));
			else
				return res.json(message.updateSuccess());
		});
	});
});

module.exports = router;
