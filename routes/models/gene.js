var express = require('express');
// var config = require('../../config.json');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var router = express.Router();

router.get('/exist', function(req, res, next) {
	var gene = req.query.gene;

	getConnection(function(connection) {
		connection.query('select CGIS.sf_isHugoSymbol(?) as exist', [gene], function(err, rows) {
			if (err) return next(err);

			res.json(rows[0]);
		});
	});
});

module.exports = router;
