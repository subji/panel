var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');
var util = require('../modules/util');
var security = require('../modules/security');
var getConnection = require('../modules/mysql_connection');

// Only Admin can access whole user data.
router.get('/', security.isAdmin, function(req, res, next) {
  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getUsers()', function(err, rows) {
      if (err) return next(err);
      res.json(rows[0]);
    });
  });
});

/*
 * 보안 상 파라미터로 받지 않고, 로그인한 정보로 사용자 정보를 리턴한다.
 */
router.get('/profile', function(req, res, next) {
  getConnection(function(connection) {
    connection.query("call CGIS.sp_getUserByName(?)", [req.user.username], function(err, rows, fields) {
      if (err) return next(err);
      var profile = rows[0][0];

      res.render('system/profile', {
        user: req.user,
        profile: profile
      });
    });
  });
});

router.put('/', function(req, res, next) {
  var pk = req.body.pk;
  var colname = req.body.name;
  var colvalue = req.body.value;

  // console.log(pk,colname,colvalue);

  // Check Primary Key
  if (util.isUndefined(pk, colname, colvalue))
    return res.status(400).send('ERRPK:Bad Request!!!');

  // Password 변경 로직, 보안을 위해 컬럼이름을 숨긴다.
  if(colname === 'password')
      return res.status(400).send('ERRPK:Bad Request!!!');
  if (colname === 'p') {
    colname = 'password';
    colvalue = bcrypt.hashSync(colvalue, null, null);
  }

  getConnection(function(connection) {
    connection.query('update CGIS.CM_USERS_TB set ?? = ? where id = ?', [colname, colvalue, pk], function(err, rows) {
      // Check SQL Statement
      if (err) {
        // console.log(err);
        return res.status(500).send(err.code);
      }

      // Check whether affected or not
      // console.log(rows);
      if (rows.changedRows != 1) {
        // console.log(rows);
        return res.status(500).send(rows.changedRows + ' rows affected.');
      }

      // Finally Return
      res.json({
        message: 'Updated!'
      });
    });
  });
});

module.exports = router;
