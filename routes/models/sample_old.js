var express = require('express');
var config = require('../../config.json');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var https = require("https");
var querystring = require('querystring');
var exec = require('child_process').exec;
var multer = require('multer');
var async = require('async');
var router = express.Router();

var panel_script = '/data/pipeline/panel/bin/run.sh';

router.get('/list', function(req, res, next) {
	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getSampleList()', function(err, rows) {
			if (err) return next(err);
			res.json(rows[0]);
		});
	});
});

var ifnull = function(data, value) {
	return (data === undefined) ? value : data;
};

var registerBiobank = function(sample, cb) {
	var postData = querystring.stringify(sample);

	var options = {
		hostname: config.biobank.host,
		path: '/openapi/sample',
		method: 'POST',
		// 아래 3줄은 curl --insecure와 같이 Self signed를 위해 필요한 코드 이다.
		rejectUnauthorized: false,
		requestCert: true,
		agent: false,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': postData.length
		}
	};
	var data = [];
	var req = https.request(options, function(res) {
		//   console.log('STATUS: ${res.statusCode}');
		//   console.log(HEADERS: ${JSON.stringify(res.headers)});
		// res.setEncoding('utf8');
		res.on('data', function(chunk) {
			data.push(chunk);
		});
		res.on('end', function() {
			var buffer = Buffer.concat(data);
			var str = buffer.toString('utf8');
			console.log('No more data in response.', str);
			cb(null, JSON.parse(str));
		});
	});

	req.on('error', function(e) {
		console.log('problem with request:', e);
		cb(e);
	});

	// write data to request body
	req.write(postData);
	req.end();
};

router.get('/', function(req, res, next) {
	var sample_id = req.query.sample_id;

	if (util.isUndefined(sample_id))
		return res.status(400).send('Not sufficient paramerters');

	getConnection(function(connection) {
		connection.query('CALL sp_getSample(?)', [sample_id], function(err, rows) {
			if (err)
				return next(err);
			res.render('menu/sample/sample', {
				sample: rows[0][0],
				sequencingHistory: rows[1],
				analysisHistory: rows[2],
				clinical: rows[3][0],
				categories: rows[4],
			});
		});
	});
});

router.post('/', function(req, res, next) {
	var sample = req.body;
	var user = req.user;
	// console.log(sample, user);
	if (util.isUndefined(sample, user))
		return res.status(400).send('Not sufficient paramerters');
	var options = [
		user.id,
		user.fullname,
		user.institute_short,
		sample.sequencing_inst,
		sample.cancer_type,
		ifnull(sample.wes_normal, '0'),
		ifnull(sample.wes_tumor, '0'),
		ifnull(sample.wts_normal, '0'),
		ifnull(sample.wts_tumor, '0'),
		ifnull(sample.wes_normal_r1_filename, ''),
		ifnull(sample.wes_normal_r2_filename, ''),
		ifnull(sample.wes_tumor_r1_filename, ''),
		ifnull(sample.wes_tumor_r2_filename, ''),
		ifnull(sample.wts_normal_r1_filename, ''),
		ifnull(sample.wts_normal_r2_filename, ''),
		ifnull(sample.wts_tumor_r1_filename, ''),
		ifnull(sample.wts_tumor_r2_filename, ''),
		sample.comments
	];
	// console.log('options',options);

	getConnection(function(connection) {
		connection.query('select CGIS.sf_registerSample(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) as sample_id', options, function(err, rows) {
			// if (err) return next(err);
			if (err) {
				res.json({
					code: 1,
					message: 'error when insert data on cgis.',
					error: err
				});
			} else {
				sample.sample_id = rows[0].sample_id;
				sample.requester_inst = user.institute_short;
				sample.requester = user.fullname;
				// console.log('1', sample);

				registerBiobank(sample, function(err, row) {
					// if (err) return next(err);
					row.sample = sample;
					if (err) res.json({
						code: 2,
						message: 'error when insert data on biobank.',
						error: err
					});
					else res.json(row);
				});
			}
		});
	});
});

router.get('/getSampleVariantList', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.query.cancer_type;
	var sample_id = req.query.sample_id;
	var frequency = req.query.frequency;
	var classification = req.query.classification;
	var cosmic = req.query.cosmic;
	var driver = req.query.driver;
	var filter_option = req.query.filter_option;

	if (util.isUndefined(source, cancer_type, sample_id, frequency, classification, cosmic, driver, filter_option))
		return res.status(400).send('Not sufficient paramerters');

	getConnection(function(connection) {
		var params = [source, cancer_type, sample_id, classification, cosmic, driver, frequency, filter_option];
		connection.query('CALL CGIS.sp_getSampleVaiants(?,?,?,?,?,?,?,?)', params, function(err, rows) {
			if (err) return next(err);
			items = rows[0];
			// Mining Drugs
			async.each(items, function(item, cb) {
				connection.query('CALL CGIS.sp_getMiningDrugList(?,?,?,?)', [item.cancer_type, item.gene, item.alt, item.cds], function(err, rows) {
					if (err) cb(err);
					item.miningdrugs = rows[0];
					// OncoKB Drugs
					connection.query('CALL CGIS.sp_getOncokbDrugList(?,?)', [item.gene, item.alt], function(err, rows) {
						if (err) cb(err);
						item.oncokbdrugs = rows[0];
						connection.query('CALL CGIS.sp_getFDAApprovedDrugForGene(?,?)', [item.cancer_type, item.gene], function(err, rows) {
							if (err) cb(err);
							item.fda_in_gene_drugs = rows[0];
							connection.query('CALL CGIS.sp_getFDAApprovedDrugForAlt(?,?,?,?)', [item.cancer_type, item.gene, item.alt, item.cds], function(err, rows) {
								if (err) cb(err);
								item.fda_in_alt_drugs = rows[0];
								cb();
							});
						});
					});
				});
			}, function(err) {
				if (err) return next(err);
				res.json(items);
			});
		});
	});
});

router.put('/clinical', function(req, res, next) {
	var cancer_type = req.body.cancer_type;
	var pk = req.body.pk;
	var colname = req.body.name;
	var colvalue = req.body.value;

	// Check Primary Key & cancer_type
	if (util.isUndefined(cancer_type, pk, colname, colvalue))
		return res.status(400).send('ERRPK:Bad Request!!!');

	getConnection(function(connection) {
		var table_name = 'GS_CLINICAL_' + cancer_type.toUpperCase() + '_TB';

		connection.query('update ?? set ?? = ? where sample_id = ?', [table_name, colname, colvalue, pk], function(err, rows) {
			// Check SQL Statement
			if (err) {
				console.log(err);
				return res.status(500).send(err.code);
			}

			// Check whether affected or not
			// console.log(rows);
			if (rows.changedRows != 1) {
				// console.log(rows);
				return res.status(500).send(rows.changedRows + ' rows affected.');
			}

			// Finally Return
			res.json({
				message: 'Updated!'
			});
		});
	});
});

/**
 * Clinical Data를 업로드한다.
 */
router.post('/clinical', function(req, res, next) {
	var data = req.body;
	var cancer_type = data.cancer_type;
	var row = JSON.parse(data.row);
	if (util.isUndefined(cancer_type, row))
		return res.status(400).send('ERRPK:Bad Request!!!');

	getConnection(function(connection) {
		var table_name = 'GS_CLINICAL_' + cancer_type.toUpperCase() + '_TB';
		connection.query('insert into ?? set ?', [table_name, row], function(err) {
			if (err) res.json({
				type: 'error',
				msg: err.message
			});
			else res.json({
				type: 'success'
			});
		});
	});
});

module.exports = router;
