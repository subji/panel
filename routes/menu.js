var express = require('express');
var passport = require('passport');
var util = require('./modules/util');
var getConnection = require('./modules/mysql_connection');
var router = express.Router();

// middleware for menu
router.use(function(req, res, next) {
	res.locals.analysis = req.session.analysis;
	next();
});

router.get('/', function(req, res, next) {
	res.render('menu/index');
});
// Cancer Panel Analysis 화면에서 샘플 하나를 눌렀을 때, cohort 의 값을
// 계산하고 이쪽으로 analysis id 가 파라미터로 온다.
router.get('/analysis/first', function(req, res, next) {
	var analysis_id = req.query.analysis_id;

	if (util.isUndefined(analysis_id))
		return res.redirect('/menu');

	// init sample session
	req.session.analysis = {};

	// Get Sample Basic Info for Session.
	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getAnalysisBasic(?)', [analysis_id], function(err, rows) {
			if (err)
				return next(err);
			// 세션에 analysis_id (rows[0][0]) 과 
			// GS_COHORT_SUBTYPE_TB 의 source 칼럼의 리스트(ex] ["GDAC", "SMC"])를 받는다
			req.session.analysis = rows[0][0];
			req.session.analysis.cohort_list = rows[1];
			// 세션 저장 후 서머리 화면으로 리다이렉팅한다.
			res.redirect('summary');
		});
	});
});
// Cancer Panel Analysis 에서 analysis id 를 누르고 나면 
// summary 로 리다이렉팅이 되는 데 그 부분이 여기다.
router.get('/analysis/summary', function(req, res, next) {
	//req.session.sample.source 은 cohort를 저장하면 /models/cohort/filter 에서 사용자가 지정한 소스로 저장한다.
	//없으면 Default 는 GDAC

	var source = req.session.analysis.source || 'GDAC';
	var cancer_type = req.session.analysis.cancer_type;
	var analysis_id = req.session.analysis.analysis_id;

	if (util.isUndefined(source, cancer_type, analysis_id))
		return res.status(400).send('Not sufficient paramerters');

	var patient = {
		clinical: {},
		categories: {},
		genomic_alt: 0,
		mutational_cd: 0,
		cosmic_cgc: 0,
		fda_cancer: 0,
		fda_other_cancer: 0,
		clinical_trials: 0,
		implications: []
	};

	getConnection(function(connection) {
		// Summary 화면에 뿌려줄 데이터를 가져오는 스토어드프로시져 호출
		connection.query('CALL CGIS.sp_getAnalysisSummary(?,?,?)', [source, cancer_type, analysis_id], function(err, rows) {
			if (err)
				return next(err);

			patient.clinical = rows[0][0];
			patient.categories = rows[1];
			var list = rows[2];
			patient.genomic_alt = 0;
			patient.alt = list.length;
			var prev_gene = '';

			list.forEach(function(item, index) {
				console.log(item);
				if (item.gene === prev_gene) item.isSameGene = true;
				else item.isSameGene = false;

				if (!item.isSameGene) { // Gene Level
					patient.genomic_alt++;
					if (item.mdAnderson > 0)
						patient.mutational_cd++;
					if (item.fda_cancer !== null)
						patient.fda_cancer += item.fda_cancer.split(',').length;
					if (item.fda_other_cancer !== null)
						patient.fda_other_cancer += item.fda_other_cancer.split(',').length;
					if (item.clinical_trials !== null)
						patient.clinical_trials += item.clinical_trials.split(',').length;
				}
				// Varialts Level
				if (item.countOfCOSMIC > 0)
					patient.cosmic_cgc++;

				prev_gene = item.gene;
			});
			patient.implications = list;
			// console.log(list);
			res.render('menu/analysis/summary', patient);
		});
	});
});
// 좌측 Cohort Selection 버튼을 눌렀을 때 이쪽으로 요청을 받아
// source (GDAC) 와 cancer_type 을 파라미터로 
// DB 에서 데이터를 받아 리스트로 반환한다.
router.get('/analysis/globalsetting', function(req, res, next) {
	var source = req.query.source;
	var cancer_type = req.session.analysis.cancer_type;

	if (util.isUndefined(source, cancer_type))
		return res.status(400).send('Not sufficient paramerters');

	getConnection(function(connection) {
		connection.query('CALL CGIS.sp_getCohortSubType(?,?)', [source, cancer_type], function(err, rows) {
			if (err)
				return next(err);

			// console.log('The solution is: ', rows);
			var subtypelist = rows[0];
			var items = rows[1];
			// subtype list에 해당 subtype 을 list에 push한다.
			items.forEach(function(item) {
				subtypelist.forEach(function(subtype) {
					if (subtype.subtype === item.subtype) {
						if (!subtype.list)
							subtype.list = [];
						subtype.list.push(item);
					}
				});
			});
			// console.log(subtypelist);
			res.render('menu/analysis/globalsetting', {
				subtypelist: subtypelist
			});
		});
	});
});
// ":" 는 와일드카드로서 page 부분에 적혀진 url 로 연결된다.
router.get('/analysis/:page', function(req, res, next) {
	var loc = 'menu/analysis/' + req.params.page;
	res.render(loc);
});

router.get('/population/:page', function(req, res, next) {
	var loc = 'menu/population/' + req.params.page;
	res.render(loc);
});
// View folder 아래 파라미터로 받은 페이지로 이동.
router.get('/sample/:page', function(req, res, next) {
	var loc = 'menu/sample/' + req.params.page;
	res.render(loc);
});

router.get('/panel', function(req, res, next) {
	res.render('menu/panel/index');
});

router.get('/panel/:page', function(req, res, next) {
	var loc = 'menu/panel/' + req.params.page;
	res.render(loc);
});
// Cancer Panel Analysis 화면에서 Edit 버튼을 눌렀을때 라우팅을 받는 코드.
router.get('/panel/clinical/:cancer_type/:analysis_id', function(req, res, next) {
	var cancer_type = req.params.cancer_type;
	var analysis_id = req.params.analysis_id;
	console.log(cancer_type, analysis_id);
	// var loc = 'menu/panel/' + req.params.cancer_type;

	getConnection(function(connection) {
		connection.query('CALL sp_getAnalysisClinical(?,?)', [cancer_type, analysis_id], function(err, rows) {
			if (err)
				return next(err);
			// GS_CLINICAL_(cancer_type)_TB 에서 파라미터로 전송한 
			// analysis_id 에 맞는 데이터를 불러온다.
			// 이 부분은 암 종 별로 보여지는 column 이 다르다.
			var clinical = rows[0][0] || {};
			// 아래의 화면으로 렌더링 된다.
			res.render('menu/panel/clinical/clinical', { cancer_type: cancer_type, analysis_id: analysis_id, clinical: clinical });
			// res.render('menu/panel/clinical/' + cancer_type, { cancer_type: cancer_type, analysis_id: analysis_id, clinical: clinical });
		});
	});
});

/* table을 생성하기 위한 것. */
router.get('/panel/:cancer_type/:analysis_id', function(req, res, next) {
	var cancer_type = req.params.cancer_type;
	var analysis_id = req.params.analysis_id;
	// console.log(cancer_type, analysis_id);
	// var loc = 'menu/panel/' + req.params.cancer_type;

	getConnection(function(connection) {
		connection.query('CALL sp_getAnalysisClinicalforForm(?,?)', [cancer_type, analysis_id], function(err, rows) {
			if (err)
				return next(err);
				console.log(rows[0][0],rows[1]);
			res.render('menu/panel/clinical', {
				clinical: rows[0][0],
				categories: rows[1],
			});
		});
	});
});

module.exports = router;
