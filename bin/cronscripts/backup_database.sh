#!/bin/bash
# 매일 한번 실행하는 DB Full Backup이다.
TODAY=$(date +"%Y%m%d_%H%M%S")

echo '------------- Start DB Backup: ' `date +"%m/%d %H:%M:%S"`
systemctl stop mysqld.service
# 콜드 백업 방법.
cp -R /data/mysql /data/backup/mysql_$TODAY
systemctl start mysqld.service

# 일주일치만 저장함.
ROOT='/data/backup/'
#DIRS=$(ls -t1 /data/backup/)
# -t1 : sorting
DIRS=$(ls -t1 $ROOT)

declare -i cnt=1
for dir in $DIRS
do
    if [ $cnt -gt 7 ]; then
        echo '일주일전 Backup file 삭제: ' $ROOT$dir
        rm -rf $ROOT$dir
    fi
    cnt=$cnt+1
done
echo '------------- End DB Backup: ' `date +"%m/%d %H:%M:%S"`
