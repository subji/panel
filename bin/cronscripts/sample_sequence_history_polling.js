#!/usr/bin/env node

/**
 * 이 프로그램은 Biobank로부터 Sample Sequencing Status를 읽어 저장한다.
 * cron이 이 프로그램을 실행한다.
 * cron 확인은 crontab -e
 * 로그는 /tmp/crontab.log에 저장한다.
 * @constructor J.H.Kim
 */

var https = require('https');
var moment = require('moment');
var async = require('async');
var multiline = require('multiline');
var config = require('../../config.json');
var getConnection = require('../../routes/modules/mysql_connection');
var url = config.biobank.url.https + '/openapi/sequencing?institute_id=' + config.institute_id;
//TODO Self Signed Error 때문에 설정함. 공인 CA를 통한 https 요청시에는 불필요함.
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

var title = 'Sample Sequencing History Polling';

var sql = multiline(function() {
  /*
    insert into PM_SEQUENCING_STATUS_HISTORY_TB
    (sample_id,
    sequencing_type,
    tissue_type,
    status_cd,
    seq_no,
    message,
    modify_user,
    modify_date)
    values (?,?,?,?,?,?,?,?)
  */
});
var end_message = function(type, msg) {
  if (type) console.log(type, msg);
  console.log('-------------   End at %s (%s)', moment().format('MM/DD HH:mm:ss'), title);
};

console.log('\n------------- Start at %s (%s)', moment().format('MM/DD HH:mm:ss'), title);
https.get(url, function(res) {
  var data = [];
  res.on('data', function(chunk) {
    data.push(chunk);
  }).on('end', function() {
    var buffer = Buffer.concat(data);
    var str = buffer.toString('utf8');
    var histories;
    try {
      histories = JSON.parse(str);
    } catch (err) {
      return end_message('JSON Parsing Error', err);
    }
    getConnection(function(connection) {
      console.log('# of histories : %d', histories.length);
      async.eachSeries(histories, function(history, done) {
        connection.query(sql, [
          history.sample_id,
          history.sequencing_type,
          history.tissue_type,
          history.status_cd,
          history.seq_no,
          history.message,
          history.modify_user,
          history.modify_date
        ], function(err, result) {
          if (err)
            console.log('DB Error', JSON.stringify(history));
          else
            console.log('Success', JSON.stringify(history));
          done(); //done(err);로 하지 않은 이유, 에러가 나도 그 다음을 계속 실행.
        });
      }, function(err) { // done(err)로 호출하지 않기 때문에, errr가 없다.
        end_message();
        process.exit();
      });
    });
  });
}).on('error', function(err) {
  end_message('Https Error', err);
  process.exit();
});
