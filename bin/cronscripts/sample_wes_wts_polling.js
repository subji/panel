#!/usr/bin/env node

/**
 * 이 프로그램은 Biobank로부터 WES 분석이 완료된 Sample 목록을 읽어 CGIS에 저장한다.
 * CGIS에 저장하고 난 다음 Biobank에 해당 sample에 대하여 downlog_flag를 1로 수정하는 요청을 보낸다.
 * cron이 이 프로그램을 실행한다.
 * cron 확인은 crontab -e
 * 로그는 /tmp/crontab.log에 저장한다.
 * @constructor J.H.Kim
 */

var https = require('https');
var moment = require('moment');
var async = require('async');
var config = require('../../config.json');
var sync_sample_wes_variants = require('./modules/sync_sample_wes_variants');
var sync_sample_wts_results = require('./modules/sync_sample_wts_results');
//TODO Self Signed Error 때문에 설정함. 공인 CA를 통한 https 요청시에는 불필요함.
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

var url = config.biobank.url.https + '/openapi/sample/analysis_complete_list?institute_id=' + config.institute_id;
var title = 'WES/WTS Polling';

var end_message = function(type, msg) {
  if (type) console.log(type, msg);
  console.log('-------------   End at %s (%s)', moment().format('MM/DD HH:mm:ss'), title);
};

console.log('\n------------- Start at %s (%s)', moment().format('MM/DD HH:mm:ss'), title);
https.get(url, function(res) {
  var data = [];
  res.on('data', function(chunk) {
    data.push(chunk);
  }).on('end', function() {
    var buffer = Buffer.concat(data);
    var str = buffer.toString('utf8');
    var samples;
    try {
      samples = JSON.parse(str);
    } catch (err) {
      return end_message('JSON Parsing Error', err);
    }
    console.log('# of samples : %d', samples.length);
    async.eachSeries(samples, function(sample, done) {
      console.log('insert type: %s, sample id: %s:', sample.analysis_type, sample.sample_id);
      if (sample.analysis_type === 'wes')
        sync_sample_wes_variants(sample.sample_id, done);
      else if (sample.analysis_type === 'wts')
        sync_sample_wts_results(sample.sample_id, done);
    }, function(err) {
      if (err)
        console.log('Error', err);
      // console.log('------------- End (%s)', title);
      end_message();
      process.exit();
    });
  });
}).on('error', function(err) {
  end_message('Https Error', err);
  process.exit();
});
