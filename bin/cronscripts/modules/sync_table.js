/**
 * 이 프로그램은 Biobank로부터 버전업된 Table을 읽어 저장한다.
 * @constructor J.H.Kim
 */

var http = require('http');
var async = require('async');
var fs = require('fs');
var path = require('path');
var zlib = require('zlib');
var moment = require('moment');
var exec = require('child_process').exec;
var config = require('../../../config.json');
var getConnection = require('../../../routes/modules/mysql_connection');

var biobank_db = 'BIOBANK';

/**
 * 테이블 이름을 변경한다. 작업하는 모든 테이블은 Biobank 스키마 소속이라 가정한다.
 * @param  {string}   old_name          바꾸고자 하는 테이블 이름
 * @param  {string}   backup_table_name 새로운 이름
 * @param  {Function} done              Callback 함수
 */
function rename_table(old_name, backup_table_name, done) {
  getConnection(function(connection) {
    connection.query('use ??', [biobank_db], function() {
      connection.query('RENAME TABLE ?? TO ??', [
        old_name, backup_table_name
      ], function(err, result) {
        done(err);
      });
    });
  });
}

/**
 * mysql 프로그램을 사용하여 데이터를 임포트한다.
 * @param  {string}   filename 파일이름
 * @param  {Function} done     Callback 함수
 */
function import_table(filename, done) {
  var command = 'mysql -u ' + config.local_db.user + ' -h ' + config.local_db.host + ' -p' + config.local_db.password + ' ' + biobank_db + ' < ' + filename;

  var child = exec(command, function(error, stdout, stderr) {
    if (error !== null) {
      console.log('Rebuild Error: ' + error);
      console.log('stdout: ' + stdout);
      console.log('stderr: ' + stderr);
      done(error);
    }
    done();
  });

}

/**
 * 테이블 버전을 BIOBANK.PD_VERSION_TB에 업데이트한다.
 * @param  {string}   name 테이블 명.
 * @param  {[type]}   ver  버전
 * @param  {Function} done Callback 함수
 */
function update_table_version(name, ver, done) {
  getConnection(function(connection) {
    connection.query('update BIOBANK.PD_VERSION_TB set table_version = ? where table_name = ?', [
      ver, name
    ], function(err, result) {
      if (result.changedRows !== 1)
        console.log('PD_VERSION_TB : %d row Updated', result.changedRows);
      done(err);
    });
  });
}

/**
 * 이 프로그램은 Biobank로부터 버전업된 Table을 읽어 Sync하는 프로그램이다.
 * 작업 순서
 *  1. 기존 Table 이름 변경(백업용)
 *  2. 새 테이블 Import.
 *  3. 새 테이블 버전 Update.
 * @param  {Table}  biobank_table Sync할 Biobank 테이블 객체  Table: {name,ver}
 * @param  {Table}  cgis_table    Sync 대상 Cgis 테이블 객체
 * @param  {[type]} finish        Callback 함수
 */
module.exports = function(biobank_table, cgis_table, finish) {
  if (!biobank_table || !cgis_table)
    throw new Error('Null parameter on sync_table');
  var unzip = zlib.createGunzip();
  var filename = biobank_table.name + '.' + biobank_table.ver + '.sql'; //저장할 파일 이름.
  var url = config.biobank.url.http + '/data/database/' + filename + '.gz'; // 서버 상 URL, 압축되어 있다.
  console.log("\t%s,Download from file: %s", moment().format('HH:mm:ss'), url);

  // 저장할 로컬 디렉토리, 현재 모듈이 있는 경로로부터 상대경로를 가져와야 함.
  var dir = path.join(__dirname, '../download/');

  var file = fs.createWriteStream(dir + filename);
  var request = http.get(url, function(res) {
    if (res.statusCode !== 200)
      finish(new Error('Http Request Error:' + res.statusCode + ':' + res.statusMessage + ',url:' + url));
    res.pipe(unzip).pipe(file);
    file.on('finish', function() {
      file.close(function() {
        // Main Logic
        var backup_table_name = cgis_table.name + '_' + cgis_table.ver.replace('.', '_');
        async.waterfall([
          // 1. 기존 Table 이름 변경
          function(done) {
            console.log('\t%s,Rename table: %s -> %s', moment().format('HH:mm:ss'), cgis_table.name, backup_table_name);
            rename_table(cgis_table.name, backup_table_name, done);
          },
          // 2. 새 테이블 Import
          function(done) {
            console.log('\t%s,Import table from: %s', moment().format('HH:mm:ss'), dir + filename);
            import_table(dir + filename, done);
          },
          // 3. 새 테이블 버전 Update.
          function(done) {
            console.log('\t%s,Update table version: %s -> %s', moment().format('HH:mm:ss'), cgis_table.ver, biobank_table.ver);
            update_table_version(cgis_table.name, biobank_table.ver, done);
          }
        ], function(err, results) {
          // async.waterfall 완료 함수.
          if (err) {
            // Error가 있으면 테이블 이름을 제자리로 돌려 놓는다. 혹 이름이 바뀌지 않아도 백업 테이블 이름이 존재하지 않으므로 무해하다.
            rename_table(backup_table_name, cgis_table.name, function() {
              finish(err);
            });
          } else {
            finish();
          }
        });
      });
    });
  }).on('error', function(err) {
    console.log('error', err);
    finish(err);
  });
};
