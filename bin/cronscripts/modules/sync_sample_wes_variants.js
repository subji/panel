/**
 * 이 프로그램은 Biobank로부터 Sample WES 분석결과(WES_VARIANTS/TRANSCRIPTS)를 읽어 저장한다.
 * @constructor J.H.Kim
 */

var https = require('https');
var http = require('http');
var path = require('path');
var fs = require('fs');
var async = require('async');
var multiline = require('multiline');
var config = require('../../../config.json');
var getConnection = require('../../../routes/modules/mysql_connection');
//TODO Self Signed Error 때문에 설정함. 공인 CA를 통한 https 요청시에는 불필요함.
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

var insert_variant_sql = multiline(function() {
    /*
        INSERT INTO CGIS.GS_WES_VARIANTS_TB
        (line,classification,hugo_symbol,chromosome,start_pos,end_pos,ref,variant,vaf,sample_id)
        values (?,?,?,?,?,?,?,?,?,?)
    */
});

/**
 * variants 배열을 Database에 입력한다.
 * @param [variant] variants - variant array
 * @param function done - 작업 완료 후 호출할 Callback
 */
var insert_variants = function(variants, done) {
    getConnection(function(connection) {
        async.eachSeries(variants, function(variant, cb) {
            connection.query(insert_variant_sql, [
                variant.line,
                variant.classification,
                variant.hugo_symbol,
                variant.chromosome,
                variant.start_pos,
                variant.end_pos,
                variant.ref,
                variant.variant,
                variant.vaf,
                variant.sample_id
            ], function(err, result) {
                cb(err);
            });
        }, function(err) {
            if (!err)
                console.log("\t insert %d variants.", variants.length);
            done(err);
        });
    });
};

/**
 * 해당 Sample의 variants 를 Database에서 삭제한다.
 * @param {string} sample_id - Sample ID
 * @param function done - 작업 완료 후 호출할 Callback
 */
var delete_variants = function(sample_id, done) {
    getConnection(function(connection) {
        connection.query('delete from GS_WES_VARIANTS_TB where sample_id = ?', [sample_id], function(err, result) {
            if (!err)
                console.log("\t delete %d variants.", result.affectedRows);
            done(err);
        });
    });
};

var insert_transcripts_sql = multiline(function() {
    /*
        INSERT INTO CGIS.GS_WES_VARIANTS_TRANSCRIPTS_TB
        (line,hugo_symbol,accession_no,uniprot_acc_id,exon_id,mutation_cds,mutation_aa,sample_id)
        values (?,?,?,?,?,?,?,?)
    */
});

/**
 * transcripts 배열을 Database에 입력한다.
 * @param [transcript] transcripts - transcript array
 * @param function done - 작업 완료 후 호출할 Callback
 */
var insert_transcripts = function(transcripts, done) {
    getConnection(function(connection) {
        async.eachSeries(transcripts, function(transcript, cb) {
            connection.query(insert_transcripts_sql, [
                transcript.line,
                transcript.hugo_symbol,
                transcript.accession_no,
                transcript.uniprot_acc_id,
                transcript.exon_id,
                transcript.mutation_cds,
                transcript.mutation_aa,
                transcript.sample_id
            ], function(err, result) {
                cb(err);
            });
        }, function(err) {
            if (!err)
                console.log("\t insert %d transcripts.", transcripts.length);
            done(err);
        });
    });
};

/**
 * 해당 Sample의 transcripts 를 Database에서 삭제한다.
 * @param {string} sample_id - Sample ID
 * @param function done - 작업 완료 후 호출할 Callback
 */
var delete_transcripts = function(sample_id, done) {
    getConnection(function(connection) {
        connection.query('delete from GS_WES_VARIANTS_TRANSCRIPTS_TB where sample_id = ?', [sample_id], function(err, result) {
            if (!err)
                console.log("\t delete %d transcripts.", result.affectedRows);
            done(err);
        });
    });
};

/**
 * 갤럭시 서버로 부터 해당 샘플의 bam, bai 파일을 다운로드한다.
 * @param {string} filename - 다운로드할 파일 이름
 * @param function done - 작업 완료 후 호출할 Callback
 */
var download_and_save_file = function(filename, done) {
    // 파일이 있는 갤럭시 서버 URL
    var url = config.galaxy.url.http + '/download/bam/';
    // 저장할 로컬 디렉토리, 현재 모듈이 있는 경로로부터 상대경로를 가져와야 함.
    var local_directory = path.join(__dirname,'../../../public/data/bam/');

    http.get(url + filename, function(res) {
        if (res.statusCode !== 200)
            done(new Error('Http Request Error:' + res.statusCode + ':' + res.statusMessage + ',url:' + url + filename));
        var stream = fs.createWriteStream(local_directory + filename);
        res.pipe(stream);
        stream.on('finish', function() {
            console.log('\t finish downloading: %s', filename);
            done();
        });
    }).on('error', function(err) {
        done(err);
    });
};

/**
 * 해당 Sample 동기화 작업이 완료되었음을 서버에 알린다. 서버는 wes_download_flag를 1로 바꾼다.
 * @param {string} sample_id - Sample ID
 * @param function done - 작업 완료 후 호출할 Callback
 */
var notify_wes_sync_complete = function(sample_id, done) {
    var url = config.biobank.url.https + '/openapi/sample/wes_sync_complete?sample_id=' + sample_id;
    // console.log("\t url", url);
    https.get(url, function(res) {
        var data = [];
        res.on('data', function(chunk) {
            data.push(chunk);
        }).on('end', function() {
            var buffer = Buffer.concat(data);
            var str = buffer.toString('utf8');
            var result;
            try {
                result = JSON.parse(str);
            } catch (err) {
                done(err);
            }
            console.log("\t notify_wes_sync_complete", result.message);
            if (0 === result.code)
                done();
            else
                done(new Error(result.message));
            }
        );
    }).on('error', function(err) {
        done(err);
    });

};

/**
 * WES 분석이 완료된 Sample의 Variants, Transcripts를 서버와 동기화하고, 동기화가 완료되면 이를 서버에 알린다.
 * @param {string} sample_id - Sample ID
 * @param function finish - 작업 완료 후 호출할 Callback
 */
module.exports = function(sample_id, finish) {
    if (!sample_id)
        finish(new Error('No Sample Id'));

    var url = config.biobank.url.https + '/openapi/sample/wes?sample_id=' + sample_id;
    console.log("\t url", url);
    https.get(url, function(res) {
        var data = [];
        res.on('data', function(chunk) {
            data.push(chunk);
        }).on('end', function() {
            var buffer = Buffer.concat(data);
            var str = buffer.toString('utf8');
            var wes;
            try {
                wes = JSON.parse(str);
            } catch (err) {
                finish(err);
            }
            // Main Logic
            async.waterfall([
                // 1. old variants delete
                function(done) {
                    delete_variants(sample_id, done);
                },
                // 2. variant insert
                function(done) {
                    insert_variants(wes.variants, done);
                },
                // 3. old transcripts delete
                function(done) {
                    delete_transcripts(sample_id, done);
                },
                // 4. transcripts insert
                function(done) {
                    insert_transcripts(wes.transcripts, done);
                },
                // 5. download bam file
                function(done) {
                    download_and_save_file(sample_id + ".bam", done);
                },
                // 6. download bai file
                function(done) {
                    download_and_save_file(sample_id + ".bai", done);
                },
                // 7. notify to biobank that sync completed
                function(done) {
                    notify_wes_sync_complete(sample_id, done);
                }
            ], function(err, results) {
                finish(err);
            });
        });
    }).on('error', function(err) {
        finish(err);
    });
};
