'use strict';

(function (factory)	{
	if (typeof define === 'function' && define.amd)	{
		define('waterfall', [], factory);
	} else {
		factory(waterfall);
	}
} (function ()	{
	
}));