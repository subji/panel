'use strict';

(function (factory)	{
	if (typeof define === 'function' && define.amd)	{
		define('scatter', [], factory);
	} else {
		factory(scatter);
	}
} (function ()	{
	
}));