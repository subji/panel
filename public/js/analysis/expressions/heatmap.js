'use strict';

(function (factory)	{
	if (typeof define === 'function' && define.amd)	{
		define('heatmap', [], factory);
	} else {
		factory(heatmap);
	}
} (function ()	{
	
}));