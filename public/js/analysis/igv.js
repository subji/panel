'use strict';

var Igv = (function () {
	return {
		seq_prefix: 'http://203.255.191.19/data/seq/hg19/',
		bam_prefix: '/data/bam/',
		browser: null,
		nowData: null,
		idx: 0 ,
		files: [],
		view: function (type, i, data)	{
			var that = this;
					that.idx = i;

			if (data.length === 0)	{
				if (that.browser)	{
					removeBrowser(that);
				}
				return;
			}

			setModalBody(type, +that.idx, data);
		},
		create  : function (type, i, data)	{
			var that 	= this;
			var popup = $('#changeData');
			var doc 	= $(document);

			popup.popover({
				'container': 'body',
				'title': 'Choose data',
				'content': function ()	{
					return showDataList(that);
				},
				'html': true,
			});

			showBrowser(type, i, data, that);
		},
	}
}());

var removeBrowser =  function (that) {
	$('#igvView2').html('');

	that.browser = null;

	delete igv['browser'];
}

var showBrowser = function (type, i, data, that)	{
	var chr = 'chr' + data[i].chr + ':';
	var locus = data[i].start_pos;
	var bp_str = makeComma(locus - 100) + '-' + makeComma(locus + 100);

  if (that.browser === null)	{
		var add = type === 'tab' ? '2' : '';
   	var div = $('#igvView' + add);
		var config = {
      'showNavigation': true,
      'showCenterGuide': true,
      'showCursorTrackingGuide': true,
      'genome': "hg19",
     	'locus': (chr + bp_str),
     	'flanking': 1,
      'tracks': [
         {
         		'name': data[0].analysis_id,
            'url': that.bam_prefix + data[0].analysis_id + '.bam',
            'indexURL': that.bam_prefix + data[0].analysis_id + '.bam.bai',
            'type': 'bam',
            'height': 600,
            'maxHeight': 600,
         },
      ]
	  };

   	that.browser = new igv.createBrowser(div, config);
  } else {
   	that.browser.search(chr + bp_str);
  }
}

var showDataList  = function (self) {
	var start = '<div id="dataList"><div class="list-group">';
	var end = '</div></div';
	var list = '';

	self.files.forEach(function (d)	{
		var active = self.nowData === d ? ' active' : '';

		list += '<a href="#" class="list-group-item' + active + '">' + d + '</a>'
	});

	return start + list + end;
}

var setLabelText  = function (type, idx, data) {
	var d = data[idx];

 	if (type === 'modal')	{
 		var gene  = $('#igvGene'),  chr = $('#igvChr'),
		 	  locus = $('#igvLocus'), cds = $('#igvCds'),
		 	  vaf 	= $('#igvVaf');

 		gene.text(d.gene);
	 	chr.text(d.chr);
	 	locus.text(makeComma(+(d.start_pos)));
	 	cds.text(d.cds);
	 	vaf.text(((d.vaf * 100) + '%'));
 	}
}

var setModalBody  = function (type, idx, data) {
	if (idx === 0)	{
		$('#igvPrevBtn').addClass('disabled');
	} else if (idx === (data.length - 1)) {
		$('#igvNextBtn').addClass('disabled');
	} else {
		$('#igvPrevBtn, #igvNextBtn').removeClass('disabled');
	}

	setLabelText(type, idx, data);

	Igv.create(type, idx, data);
}

var makeComma = function (locus) {
	var parts = locus.toString().split('.');
   	  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

	return parts.join('.');
}