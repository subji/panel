'use strict';

(function ()  {
  var width = 1620,
      height = 840;

  var sidebar = document.querySelector('#sidebar'),
      navbar = document.querySelector('.navbar'),
      container = document.querySelector('.container');

  var sbcr = sidebar.getBoundingClientRect(),
      nbcr = navbar.getBoundingClientRect();

  container.style.width = '1632px';
  container.style.height = '850px';
  container.style.margin = '0px';
  container.style.visibility = 'visible';

  document.querySelector('#maincontent').style.marginBottom = '0px';

  var width = parseFloat(container.style.width),
      height = parseFloat(container.style.height);

  $.ajax({
    'type': 'GET',
    'url': '/rest/mutext',
    data: {
      source: 'GDAC',
      cancer_type: 'luad',
      analysis_id: $('#analysis_id').val(),
    },
    beforeSend: function () {
      bio.loading().start(document.querySelector('#main'), 1620, 850);
    },
    success: function (d) {
      bio.exclusivity({
        element: '#main',
        width: 1620,
        height: 850,
        data: {
          heatmap: d.data.file_list[1].contents,
          network: d.data.file_list[0].contents,
          sample: d.data.sample_variants,
          survival: {
            patient: d.data.patient_list,
            types: d.data.variant_types,
          },
          type: 'LUAD',
        }
      });

      bio.loading().end();
    },
  });
}());