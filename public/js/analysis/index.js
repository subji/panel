/**
 * 로그인 후 처음 보이는 Cancer Panel Analysis 화면 스크립트.
 */
$(function() {
	// find user locale for date format
	// var locLang = (navigator.language) ? navigator.language : navigator.userLanguage;
	// var userLocale = locLang.substring(0, 2) || 'en';
	// moment.locale(userLocale);
	moment.locale('ko');

	var checkformatter = function(value, row, index) {
		return (value === 1) ? '<i class="fa fa-check"></i>' : '';
	};
	/**
	 * Status Column 의 값을 포맷하는 함수.
	 * @param {string} value 
	 * @param {object} row 
	 * @param {int} index 
	 */
	var resultFormatter = function(value, row, index) {
		if (!value) return '';
		var atag = '<a href="/models/sample?analysis_id=' + row.analysis_id + '" title="Sample status">';
		if (value === 'EX')
			return '<span class="label label-danger link_only_cursor">Error</span>';
		if (value === 'E2')
			return atag + '<span class="label label-primary step-4 link_only_cursor">All Done</span></a>';
		if (value[0] === 'E')
			return atag + '<span class="label label-primary step-3 link_only_cursor">In Analysis</span></a>';
		return value;
	};
	// FTP 관련 요청 URL
	var FTP_ROOT_DIR = '/data/public/ftpuser';
	// 화면의 Table 을 그려주는 코드.
	var table = $('#table');
	table.bootstrapTable({
		url: '/models/analysis/list',
		classes: 'table table-hover table-striped',
		method: 'get',
		search: true,
		showRefresh: true,
		showColumns: false,
		pagination: true,
		toolbar: "#toolbar",
		toolbarAlign: 'right',
		pageSize: 10,
		cookie: true,
		cookieIdTable: 'saveId',
		cookieExpire: '1mi',
		sortName: 'cnt',
		sortOrder: 'desc',
		columns: [{
			title: '#',
			align: 'center',
			formatter: function(value, row, index) {
				return index + 1;
			}
		}, {
			field: 'analysis_id',
			title: 'Analysis ID',
			sortable: true,
			align: 'center',
			formatter: function(value, row) {
				// row.analysis_flag = (row.pnl_normal_ok === 1 && row.pnl_tumor_ok === 1 && row.clinical === 'Y') ? 'Y' : 'N';
				// pnl_tumor_ok 값과 clinical 의 값이 1 과 'Y' 일 경우 analysis_flag 는 'Y' 를 아닐 경우 'N' 으로 입력.
				row.analysis_flag = (row.pnl_tumor_ok === 1 && row.clinical === 'Y') ? 'Y' : 'N';
				// 분석이 완료되면 analysis_flag 가 'Y' 값이 되며 아닐 경우 해당 태그의 링크를 없애고 일반 문자열로만 표시한다.
				return (row.analysis_flag === 'Y') ? '<span class="link" title="View analysis results">' + value + '</span>' : value;
			}
		}, {
			field: 'cancer_type',
			title: 'Type',
			sortable: true,
			align: 'center',
			formatter: function(value, row) {
				return value.toUpperCase();
			}
		}, {
			field: 'fullname',
			title: 'Requester',
			sortable: true,
			align: 'center',
		}, {
			field: 'exist_normal',
			title: 'Exist Normal?',
			align: 'center',
		}, {
			field: 'panel_id',
			title: 'Panel',
		}, {
			field: 'version',
			title: 'Ver.',
			align: 'center',
		}, {
			field: 'vcf_location',
			title: 'Files',
			formatter: function(value, row) {
				// 위에서 정의된 FTP_ROOT_DIR 을 '.' 으로 대체한 값을 반환하는 코드.
				var files = row.vcf_location.replace(FTP_ROOT_DIR, '.');
				if(row.bam_tumor_location)
					files += '<br>' + row.bam_tumor_location.replace(FTP_ROOT_DIR, '.');
				if(row.bai_tumor_location)
					files += '<br>' + row.bai_tumor_location.replace(FTP_ROOT_DIR, '.');

				if (row.exist_normal === 'Y') {
					files += row.bam_normal_location.replace(FTP_ROOT_DIR, '.') + '<br>' +
						row.bai_normal_location.replace(FTP_ROOT_DIR, '.');
				}
				return files;
			}
		}, {
			field: 'clinical_data_ref',
			title: 'Ref. ID',
			formatter: function(value, row) {
				// 두 개 전부 데이터가 없다.
				// 참조한 병원과 해당 병원의 시퀀싱 아이디 값인데 데이터가 없다.
				return 'EMR:' + row.clinical_data_ref + '<br>' +
					'Seq.:' + row.sequencing_interface_ref;
			}
		}, {
			field: 'pnl_tumor_final',
			title: '<span class="text-primary">Status</span>',
			sortable: false,
			align: 'center',
			formatter: resultFormatter
		}, {
			field: 'clinical',
			title: '<span class="text-primary">Clinical</span>',
			align: 'center',
			formatter: function(value, row, index) {
				// return (value === 'Y') ? '<a href="/models/analysis?analysis_id=' + row.analysis_id + '#clinical" title="View clinical data"><i class="fa fa-check"></i></a>' : '';
				var text = (value === 'Y') ? 'Edit' : 'Insert';
				// URL 코드를 암 종과 Analysis ID 를 합쳐서 만든다.
				// /router/menu.js 에서 163 번째 줄로 요청한다.
				return '<a href="/menu/panel/clinical/' + row.cancer_type + '/' + row.analysis_id + '">' + text + '</a>';
				// return '<a href="#">'+value+'</a>';
			}
		}, {
			field: 'comments',
			title: 'Comment',
		}, {
			field: 'insert_date',
			title: 'Req. Date',
			sortable: true,
			align: 'center',
			formatter: function(value, row, index) {
				var date = new Date(value);
				// moment 모듈의 fromNow() 함수인가 보다.
				return moment(date).fromNow();
			}
		}]
	});
	// 새로운 분석 데이터를 입력하는 버튼 이벤트 이다.
	$('#registerButton').on('click', function() {
		// 아래의 URL 로 이동한다. (view/sample/register.jade 로 리다이렉팅 된다.)
		location.href = '/menu/sample/register';
	});

	table.on('click-cell.bs.table', function(event, field, value, row, $element) {
		// console.log(field, value, row, $element);
		var source = 'GDAC'; // Default Cohort
		if (field === 'analysis_id' && row.analysis_flag === 'Y') {
			// console.log($element);
			// cohort.js 에서 cohort 의 수를 계산 후 /menu/analysis/first 로 analysis id 를 
			// 파라미터로 전송하여 summary 화면으로 리다이렉팅하게 한다.
			$.ajax({
					method: "GET",
					url: "/models/cohort",
					data: {
						source: source,
						cancer_type: row.cancer_type
					}
				})
				.done(function(cnt) {
					console.log('data', cnt);
					var params = $.param({
						analysis_id: row.analysis_id
					});
					// js/analysis/cohort.js 의 init() 함수를 사용해서
					// cohort 의 개수와 source = "GDAC" 을 저장한다.
					cohort.init(source, cnt);
					
					location.href = '/menu/analysis/first?' + params;
				})
				.fail(function(data) {
					console.log('fail', data);
				});
		} else if (field.endsWith('_final')) {

		}
	});
});
