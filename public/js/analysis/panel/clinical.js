$(function() {
	$.validator.setDefaults({
		errorElement: 'em',
		errorClass: 'text-danger',
	});
	/**
	 * 무슨 메세지를 알려주는 함수같다.
	 * 
	*/
	var noty = function(type, message) {
		$.notify({
			icon: 'glyphicon glyphicon-info-sign',
			message: message,
		}, {
			type: type,
			mouse_over: 'pause',
			placement: {
				from: "top",
				align: "center"
			},
			z_index: 1051,
			delay: 500,
			timer: 1000,
		});
	};

	var cancer_type = $('#cancer_type').val();
	var method = $('#method').val();
	// form 태그가 유효하다면 sumbit 핸들러를 이용해 register함수를 호출하는 코드.
	$("#clinical_form").validate({
		debug: true,
		submitHandler: function(form) {
			// console.log(form);
			register(form);
		},

		rules: {},
		messages: {}
	});
	// 변경된 Clinical Data 를 "PUT" 메소드로 요청한다.
	function register(form) {
		var data = $(form).serialize();
		// console.log(data);
		$.ajax({ method: method, url: '/models/clinical/' + cancer_type, data: data })
			.done(function(message) {
				console.log(message);
				if (message.success) {
					method = 'PUT';
					noty('success', message.message);
				} else
					alert(message.message);
			});
	}

});
