'use strict;';

var REPORT_TYPE = null;
var DRUG_URL = "https://www.drugbank.ca/unearth/q?searcher=drugs&approved=1&vet_approved=1&nutraceutical=1&illicit=1&withdrawn=1&investigational=1&query=";
var MUTATION_URL = "http://cancer.sanger.ac.uk/cosmic/search?q=p.";
var GENE_URL = "https://www.ncbi.nlm.nih.gov/gene?term=("; //+ 'EGFR[Sym]) AND 9606[Taxonomy ID]";
var PUBMED_SUM_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi';
var PUBMED_URL = "https://www.ncbi.nlm.nih.gov/pubmed/"; //+ 'EGFR[Sym]) AND 9606[Taxonomy ID]";
var LEVEL_LINK = "<a target='oncokb' href='http://oncokb.org/#/levels'><i class='fa fa-external-link' aria-hidden='true'></i></a>";
var NCI_URL = 'http://www.cancer.gov/about-cancer/treatment/drugs/';
var DAILYMED_URL = 'http://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=';

/**
 * Summary 하단 테이블의 Drug Popover 에서 문자열에서 '|' 로 나누었을 때,
 * drug 이름이 0 번째, 기타 데이터가 1 ~ 에 나오는 것 같다.
 * 이에 popover 에 표시될 drug 이름만 반환하는 함수.
 * popover라 링크처리하지 않음.
 * TODO 확정되면 SQL 에도 반영.
 * @param {string} value drug value string
 * @param {string} userclass badge color class name
 */
function getDrugLink(value, userclass) {
	var array = value.split('|');
	return array[0];
}
// TODO userclass를 뺐다. 제목에 색깔 표현함.
// function getDrugLink_backup(value, userclass) {
// 	var array = value.split('|');
// 	var link = '';
// 	// console.log('value', value, 'array', array);
// 	if (array.length === 3) {
// 		var url;
// 		if (array[1] === 'N') {
// 			url = 'http://www.cancer.gov/about-cancer/treatment/drugs/';
// 		} else {
// 			url = 'http://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=';
// 		}
// 		// link = '<a target=drug class=\'text-nowrap ' + userclass + '\' href=\'' + url + array[2] + '\'>' + array[0] + '</a>';
// 		link = '<a target=drug class=\'text-nowrap\' href=\'' + url + array[2] + '\'>' + array[0] + '</a>';
// 	} else {
// 		link = array[0];
// 	}
// 	// console.log(array, link);
// 	return link;
// }
/**
 * Summary 화면 하단 테이블의 FDA Approved drugs for gene 칼럼에 들어가는 내용들의
 * 숫자를 badge 형태로 그려주는 함수.
 * @param {array} value drug list
 * @param {string} delimiter delimiter ex) ',', ...
 * @param {string} title Popover title
 * @param {string} userclass badge color class name
 */
function drugPopoverFormatter(value, delimiter, title, userclass) {
	if (value === null) return '';
	delimiter = delimiter || ',';
	title = title || '';

	var targets = value.split(delimiter);
	// console.log(targets);
	if (targets.length > 0) {
		var data_content = '';
		targets.forEach(function(target) {
			data_content += getDrugLink(target, userclass) + '<br>';
		});

		var data = getDrugLink(targets[0], userclass) + ' ';
		if (targets.length > 1) {
			data += '<a tabindex="0" role="button" data-trigger="focus" data-container="body" data-placement="bottom" data-toggle="popover" data-html="true" title="' + title + '" data-content="' + data_content + '">' +
				'<span class="badge badge-link">' + targets.length + '</span></a>';
		}
		return data;
	}
}
/**
 * Summary 화면 하단 테이블의 Gene 칼럼의 데이터 옆에 외부 링크 태그를 만들어주는 함수.
 * @param {string} target external link name ex) 'ncbi', ...
 * @param {string} link external link URL
 */
function externalLink(target, link) {
	return ' <a target="' + target + '" href="' + link + '"><i class="fa fa-external-link" aria-hidden="true"></i></a> ';
}

// 화면 상단에 Variants 개수 표시해주기 위한 글로벌 변수.
var countOfAlterations = 0;
/**
 * Summary 화면에서 Therapeutic Implications 테이블을 그려주는 함수.
 * @param {array} list 
 */
function renderingTable(list) {
	var prev_gene = '';
	var slash = '<span class="text-muted"> / </span>';
	var rowspan = {};
	var cancer_type = $('#cancer_type').val();
	list.forEach(function(item) {
		rowspan[item.gene] = (rowspan[item.gene]) ? rowspan[item.gene] + 1 : 1;
	});
	// console.log(rowspan);
	list.forEach(function(item) {
		// console.log(item);
		var gene = item.gene + externalLink('ncbi', 'http://www.ncbi.nlm.nih.gov/gene?term=(' + item.gene + '[Sym]) AND 9606[Taxonomy ID]');
		if (item.mdAnderson > 0) {
			gene += '<span> </span><a href="#" data-toggle="modal" data-target="#driveModal" data-gene="' + item.gene + '"><span class="label label-danger">Driver</span></a>';
		}
		var alt = item.alt;
		if (item.countOfCOSMIC > 0) {
			alt += '<span> </span><span class="label label-primary">COSMIC</span>';
		}
		var fda_cancer = drugPopoverFormatter(item.fda_cancer, ',', 'FDA Approved for ' + cancer_type.toUpperCase(), 'agent-red');
		var fda_in_other_cancers = drugPopoverFormatter(item.fda_other_cancer, ',', 'FDA Approved for other cancer', 'agent-blue');
		var clinical_trials = drugPopoverFormatter(item.clinical_trials, ',', 'Clinical Trials', 'agent-black');
		var fda_cancer_variant = drugPopoverFormatter(item.fda_cancer_variant, ',', 'FDA Approved for ' + cancer_type.toUpperCase() + ' / ' + item.gene + ' / ' + item.mutation_aa, 'agent-blue');
		var oncokb_drugs = drugPopoverFormatter(item.oncokb_drugs, ',', 'OncoKB', 'agent-blue');

		if (prev_gene !== item.gene) {
			$('#implication_table > tbody:last-child').append(
				'<tr>' +
				'<td rowspan=' + rowspan[item.gene] + '>' + gene + '</td>' +
				// '<td rowspan=' + rowspan[item.gene] + '>' + available_drugs.join(slash) + '</td>' +
				'<td rowspan=' + rowspan[item.gene] + '>' +fda_cancer + '</td>' +
				'<td rowspan=' + rowspan[item.gene] + '>' +fda_in_other_cancers + '</td>' +
				'<td rowspan=' + rowspan[item.gene] + '>' +clinical_trials + '</td>' +
				'<td>' + alt + '</td>' +
				'<td>' + fda_cancer_variant + '</td>' +
				'<td>' + oncokb_drugs + '</td>' +
				'</tr>');
		} else {
			$('#implication_table > tbody:last-child').append(
				'<tr>' +
				'<td>' + alt + '</td>' +
				'<td>' + fda_cancer_variant + '</td>' +
				'<td>' + oncokb_drugs + '</td>' +
				'</tr>');
		}
		prev_gene = item.gene;
	});
	displayAlteration(list.length);

	$('#loading').addClass('hidden');
	$('#implication').removeClass('hidden');
	// table.on('load-success.bs.table', function(_event) {
	//   $('[data-toggle="popover"]').popover();
	// });
}

function displayAlteration(cnt) {
	// console.log(countOfAlterations);
	var alteration_string = (cnt > 1) ? ' alterations' : ' alteration';
	$('#alterations').text(' ( ' + cnt + alteration_string + ' )');
}
/*
	프린트 페이지 드러그 포매터.
*/
function drugFormatter (value, delimiter, userclass) {
	if (value === null) return '';

	var targets = value.split((delimiter = delimiter || ','));

	if (targets.length > 0) {
		var data_content = '',
			data = getDrugLink(targets[0], userclass) + ' ';

		targets.forEach(function (target) {
			data_content += getDrugLink(target, userclass) + '<br>';
		});

		if (targets.length > 1) {
			data += '<span class = "badge is-drug-print ' + userclass + '">' + targets.length + '</span>';
		}

		return data;
	}
};
/*
	프린트 페이지 테이블 렌더링
*/
function renderingTableOfPrintFromSummary (list) {
	$('#report_modal').on('show.bs.modal', function () {
		$('#implication_table_print > tbody').empty();

		var prev_gene = '';
		var slash = '<span class="text-muted"> / </span>';
		var rowspan = {};
		var cancer_type = $('#cancer_type').val();

		list.forEach(function (item) {
			rowspan[item.gene] = (rowspan[item.gene]) ? rowspan[item.gene] + 1 : 1;
		});

		list.forEach(function (item) {
			var gene = item.gene, alt = item.alt;

			if (item.countOfCOSMIC > 0) {
				var splitAlt = alt.split(' ');

				alt = splitAlt[0] + '<span class = "label label-primary is-cosmic-print">COSMIC</span></br>' +
					  splitAlt[1];
			}

			var fda_cancer = drugFormatter(item.fda_cancer, ',', 'badge-red');
			var fda_in_other_cancers = drugFormatter(item.fda_other_cancer, ',', 'badge-blue');
			var clinical_trials = drugFormatter(item.clinical_trials, ',', 'badge-black');
			var fda_cancer_variant = drugFormatter(item.fda_cancer_variant, ',', 'agent-blue');
			var oncokb_drugs = drugFormatter(item.oncokb_drugs, ',', 'agent-blue');

			if (prev_gene !== item.gene) {
				$('#implication_table_print > tbody:last-child').append(
					'<tr>' +
					'<td rowspan=' + rowspan[item.gene] + '>' + gene + '</td>' +
					'<td rowspan=' + rowspan[item.gene] + '>' + fda_cancer + '</td>' +
					'<td rowspan=' + rowspan[item.gene] + '>' + fda_in_other_cancers + '</td>' +
					'<td rowspan=' + rowspan[item.gene] + '>' + clinical_trials + '</td>' +
					'<td>' + alt + '</td>' +
					'<td>' + fda_cancer_variant + '</td>' +
					'<td>' + oncokb_drugs + '</td>' +
					'</tr>');
			} else {
				$('#implication_table_print > tbody:last-child').append(
					'<tr>' +
					'<td>' + alt + '</td>' +
					'<td>' + fda_cancer_variant + '</td>' +
					'<td>' + oncokb_drugs + '</td>' +
					'</tr>');
			}

			prev_gene = item.gene;
		});

		displayAlteration(list.length);

		$('#loading').addClass('hidden');
		$('#implication').removeClass('hidden');
	});
};
/*
	Variant 페이지 (Chart 제외) 데이터를 받아오는 함수.
*/
function getVariantData (parameters, callback)	{
	$('#report_modal').on('show.bs.modal', function () {
		$.ajax({
			url: '/models/analysis/getAnalysisVariantList',
			data: parameters,
			beforeSend: function ()	{
				var loadingPrint = $('#print_loading'),
					backWidth = $(document).width(),
					backHeight = $(document).height(),
					style = $('style');

				loadingPrint.css('width', backWidth).css('height', backHeight);
				loadingPrint.fadeIn(250);
				// 프린트 미리보기 또는 출력할 때, 상단에 보여지는 날짜와 페이지 이름을 제거한다.
				parseFloat($('#sidebar').css('margin-left')) === 0 ? 
				style.append('@page { margin-left: -150px; }') : 
				style.append('@page { margin-left: 0px; }');
			},
			success: function (data)	{
				renderingVariantsList($('#variant_table_print > tbody'), data, callback);
			},
			error: function (err) { console.error(err); },
		});
	});
};

function renderingVariantsList (tbody, list, callback)	{
	tbody.empty();

	tbody.append(
		'<tr>' +
		'<th> Gene </th>' +
		'<th> Classification </th>' +
		'<th> VAF </th>' +
		'<th> Chr </th>' +
		'<th> CDS Change </th>' +
		'<th> AA Change </th>' +
		'<th> dbSNP </th>' +
		'<th> Domain </th>' +
		'<th> Freq. in Gene </th>' +
		'<th> Freq. in Total </th>' +
		'</tr>');

	list.forEach(function (item, idx) {
		tbody.append(
			'<tr>' +
				'<td> ' + item.gene + ' </td>' +
				'<td> ' + item.class + ' </td>' +
				'<td> ' + (item.vaf * 100).toFixed(2) + '% </td>' +
				'<td> ' + item.chr + ' </td>' +
				'<td> ' + item.cds + ' </td>' +
				'<td> ' + item.alt + ' </td>' +
				'<td> ' + (item.dbsnp_rs ? item.dbsnp_rs.replace(/\|/g, '</br>') : 'NA') + ' </td>' +
				'<td> ' + (item.pdomain === '' ? '' : item.pdomain.split('|')[0]) + ' </td>' +
				'<td> ' + ((item.patientsOfPosition / item.patientsOfTranscript) * 100).toFixed(2) +
				'% (' + item.patientsOfPosition + ' / ' + item.patientsOfTranscript + ') </td>' +
				'<td> (0 / ' + item.cntOfFilteredPatient + ') </td>' +
			'</tr>'
		);
	});

	renderingDrugList($('#relevant_drug_list'), list, callback);
};

function getDrugLinkNew(drug, drugs_url, multiLink) {
	if (!drugs_url) return drug;
	var url = [];
	if (multiLink) {
		drug.split('+').forEach(function (item) {
			var link = drugs_url.shift();
			if (link && link != 'NULL') {
				link = link.replace('%NCI%', NCI_URL).replace('%DMED%', DAILYMED_URL);
				url.push('<a target=\'drug\' href=\'' + link + '\' >' + item + '</a>');
			} else {
				url.push(item);
			}
		});
	} else {
		var link = drugs_url.shift();;
		if (link && link != 'NULL') {
			link = link.replace('%NCI%', NCI_URL).replace('%DMED%', DAILYMED_URL);
			url.push('<a target=\'drug\' href=\'' + link + '\' >' + drug + '</a>');
		} else {
			url.push(drug);
		}
	}

	return url.join(' + ');
};

function renderingDrugList (target, list, availablePrinting)	{
	target.empty();

	var subtitle = $('#relevant_drugs_label'),
		beforeGene = '',
		beforeClass = '',
		beforeCDS = '',
		isNaGeneIdx = 0,
		isNaVariantIdx = 0;
	
	function drugFormatter (list, type)	{
		var resArr = [];
		// "info" 는 oncoKB druglist, success 는 mininingdrug list 이다.
		list.forEach(function (item)	{
			resArr.push({
				agent: (type === 'info' ? 'Level ' + item.level + ' - ' + item.drugs : item.drug),
				link: item.drugs_url,
				pmids: (type === 'info' ? item.pmid_for_drug : item.pmid),
				ref_pmids: (type === 'info' ? item.drugs : item.title),
				multi_link: 1,
				sentence: (type === 'info' ? '' : item.sentence)
			});
		});

		return resArr;
	}

	function drugDetail (drugList, colorType)	{
		var result = '';

		if (colorType === 'info')	{
			drugList = drugFormatter(drugList, 'info');
		} else if (colorType === 'success')	{
			drugList = drugFormatter(drugList, 'success');
		}

		drugList.forEach(function (drug)	{
			$.ajax({
				url: PUBMED_SUM_URL,
				async: false,
				data: {
					db: 'pubmed',
					retmode: 'json',
					id: drug.pmids
				},
				success: function (data)	{
					result += '<div class = "row drug-title-border"><div class = "panel panel-' + colorType + '">' +
						'<div class = "panel-heading drug-title">' + (drug.agent || drug.drug) + '</div><div class = "panel-body">';

					var drugUrl = getDrugLinkNew((drug.agent || drug.drug), (drug.link) ? drug.link.split(',') : [], drug.multi_link);
					var refs = {},
						content = '';

					if (drug.ref_pmids) {
						if (colorType === 'info')	{
							if (!refs[drug.ref_pmids])	{
								refs[drug.ref_pmids] = { pmids: [] };
							}

							drug.pmids.split(',').forEach(function (d)	{
								refs[drug.ref_pmids].pmids.push(d.trim());
							});
						} else if (colorType === 'success')	{
							if (!refs[drug.ref_pmids]) {
								refs[drug.ref_pmids] = { pmids: [] };
							}

							refs[drug.ref_pmids].pmids.push(drug.pmids.trim());
						} else {
							drug.ref_pmids.split(',').forEach(function (ref) {
								var item = ref.split('|'),
									name = item[0],
									pmid = item[1];

								if (!refs[name]) {
									refs[name] = {
										pmids: []
									};
								}

								if (pmid && pmid.trim() !== '') {
									refs[name].pmids.push(pmid);
								}
							});
						}
					} 

					Object.keys(refs).forEach(function (title) {
						title = (colorType === 'success' ? title.substring(0, 1).toUpperCase() + title.substring(1) : title);

						content += 
							'<div class="panel panel-default narrow-panel"><div class="panel-heading narrow-head">' + title + '</div>' +
							'<ul class="list-group">';

						refs[title].pmids.forEach(function (pmid) {
							content +=
								'<li class="list-group-item"><p>' + (colorType !== 'success' ? data.result[pmid].title : drug.sentence) +
								'</p><var>' + data.result[pmid].sortfirstauthor + ' et al. ' + data.result[pmid].source + 
								', ' + data.result[pmid].pubdate + '</var><span class = "pmid-drug">PMID : <b>' + pmid + '</b></span></li>';
						});

						content += '</ul></div>';

					});

					result += (content + '</div></div></div>');
				},
				error: function (err)	{
					console.error(err);
				}
			});
		});

		return result;
	}

	REPORT_TYPE === 'gene' ? subtitle.text(' Gene level') : subtitle.text(' Variant level');

	list.forEach(function (item) {
		var row = $('<div class = "row">');

		if (REPORT_TYPE === 'gene')	{
			if (item.fda_in_gene_drugs.length > 0)	{
					if (beforeGene !== item.gene) {
						row.append('<div class = "page-header col-xs-12 drug-detail-width"><h2> ' + item.gene +
							'</h2></div><hr class = "drug-header-line>');
						
						row.append(
							'<div class = "col-xs-12 drug-detail-width">' + 
							'<h2><small class = "agent-pink"> FDA Approved drugs for Gene </small></h2>' +
							drugDetail(item.fda_in_gene_drugs, 'warning') + '</div></div>'
						);

						target.append(row);

						beforeGene = item.gene;
					}
			} else {
				++isNaGeneIdx;
			}
		}	else {
			if (item.fda_in_alt_drugs.length > 0 || item.miningdrugs.length > 0 ||
				item.oncokbdrugs.length > 0) {
				if (beforeGene !== item.gene && beforeClass !== item.class && beforeCDS !== item.cds)	{
					row.append(
						'<div class = "page-header col-xs-12 drug-detail-width"><h2> ' + item.gene +
						' <small>' + item.class + ' / ' + item.cds + '</small></h2></div>' +
						'<hr class = "drug-header-line>');

					beforeGene = item.gene;
					beforeCDS = item.cds;
					beforeClass = item.class;
				}

				row.append(
					'<div class = "col-xs-12 drug-detail-width">' + 
					'<h2><small class = "agent-red"> FDA Approved drugs for Alteration </small></h2>' +
					drugDetail(item.fda_in_alt_drugs, 'danger') + '</div></div>'
				);

				target.append(row);

				row.append(
					'<div class = "col-xs-12 drug-detail-width">' + 
					'<h2><small class = "agent-blue"> OncoKB Drugs </small></h2>' + 
					drugDetail(item.oncokbdrugs, 'info') + '</div></div>'
				);

				target.append(row);

				row.append(
					'<div class = "col-xs-12 drug-detail-width">' + 
					'<h2><small class = "agent-green"> PubMed Mining Drugs </small></h2>' + 
					drugDetail(item.miningdrugs, 'success') + '</div></div>'
				);

				target.append(row);
			} else {
				++isNaVariantIdx;
			}
		}
	});

	if (isNaGeneIdx === list.length || 
		isNaVariantIdx === list.length)	{
		target.append('<div class = "row no-drug-data"><h2> There is no drug data. </h2></div>');
	}

	availablePrinting();
}

$(function() {
	// Driver Gene을 클릭했을 경우, 모달을 띄운다.
	// ! Panel 쪽에서는 사용되지 않는 기능인 것 같다.
	$('#driveModal').on('show.bs.modal', function(event) {
		var modal = $(this);
		var gene = $(event.relatedTarget).data('gene');
		$.ajax({
				method: "GET",
				url: "/models/drug/getDriveGeneInfo",
				data: {
					gene_id: gene,
				}
			})
			.done(function(data) {
				modal.find('.modal-title').html('<h3 class="modal-gene">' + gene + ' <small>Cancer Drug Target</small></h3> ');
				modal.find('.modal-body .overview').html(data[0].o_html);
				modal.find('.modal-body .alter').html(data[0].a_html);
				modal.find('.modal-body .therapeutic').html(data[0].t_html);
				// modal.find('.modal-body input').val('recipient');
			})
			.fail(function(data) {
				console.log('fail', data);
			});
	});
	// Report 버튼을 클릭 했을 때 Popover 를 띄워 준다.
	$('#report_button').popover({
		title: 'Report select',
		html: true,
		content: function ()	{
			return $('#report_selector').html();
		}
	});
	// Gene / Variant level 을 눌렀을 때, modal 을 띄워 준다.
	$(document).on('change', '.report-select-btn', function (evt)	{
		REPORT_TYPE = this.value;
		// Print preview 화면.
		$('#report_modal').modal();
	});
	// modal 이 띄워 졌을 때, print 버튼을 누르면 preview 화면이 꺼짐과 동시에 프린트 미리 보기 화면이 나온다.
	$('#report_modal').on('shown.bs.modal', function (evt)	{
		$('#report_print_btn').on('click', function ()	{
			if ($('.modal').is(':visible')) {
				$('body').css('visibility', 'hidden');
				$('#report_modal').css('visibility', 'visible');
				$('#report_modal').removeClass('modal');

				window.print();

				$('body').css('visibility', 'visible');
				$('#report_modal').addClass('modal');
			} else {
				window.print();
			}
		});
	});
});
