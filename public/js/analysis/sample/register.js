/**
 * Cancer Panel Analysis 화면에서 우측 검색 창 옆에 + 버튼을 누르면 
 * 나오는 화면의 스크립트.
 */
$(function() {

	// validator global defaults
	if ($.validator) {
		$.validator.setDefaults({
			errorElement: 'em',
			errorClass: 'text-danger',
		});
	}
	var noty = function(type, message) {
		console.log(type, message);
		$.notify({
			icon: 'glyphicon glyphicon-info-sign',
			message: message,
		}, {
			type: type,
			mouse_over: 'pause',
			placement: {
				from: "top",
				align: "center"
			},
			z_index: 1051,
			delay: 500,
			timer: 1000,
		});
	};

	var $registerForm = $('#registerForm');
	var $cancer_type = $('#cancer_type');
	var $panel = $('#panel');
	var $panel_id = $('#panel_id');
	var $version = $('#version');
	var $filter = $('#filter');
	var $vcf_location = $('#vcf_location');
	var $bam_tumor_location = $('#bam_tumor_location');
	var $bai_tumor_location = $('#bai_tumor_location');
	var $bam_normal_location = $('#bam_normal_location');
	var $bai_normal_location = $('#bai_normal_location');

	$cancer_type.select2({});

	$panel.select2({
		ajax: {
			url: '/rest/panel',
			dataType: 'json',
			processResults: function(data) {
				var d1 = _.groupBy(data, 'panel_id');
				var a = [];
				for (var key of _.keys(d1)) {
					var children = d1[key].map(function(child) {
						child.id = child.panel_id + ':' + child.version;
						child.text = child.panel_id + ' : Version ' + child.version;
						return child;
					});
					a.push({ text: key, children: children });
				}
				// console.log(data, d1,a);
				return {
					results: a
				};
			}
		}
	});

	$vcf_location.select2({
		ajax: {
			url: '/rest/user_files',
			data: function(params) {
				params.ext = 'vcf';
				params.filter = $filter.val();
				return params;
			}
		}
	});

	$bam_tumor_location.select2({
		ajax: {
			url: '/rest/user_files',
			data: function(params) {
				params.ext = 'bam';
				params.filter = $filter.val();
				return params;
			}
		}
	});

	$bai_tumor_location.select2({
		ajax: {
			url: '/rest/user_files',
			data: function(params) {
				params.ext = 'bai';
				params.filter = $filter.val();
				return params;
			}
		}
	});

	$bam_normal_location.select2({
		// disabled: true,
		ajax: {
			url: '/rest/user_files',
			data: function(params) {
				params.ext = 'bam';
				params.filter = $filter.val();
				return params;
			}
		}
	});

	$bai_normal_location.select2({
		disabled: true,
		ajax: {
			url: '/rest/user_files',
			data: function(params) {
				params.ext = 'bai';
				params.filter = $filter.val();
				return params;
			}
		}
	});

	$panel.on('select2:select', function(e) {
		var data = e.params.data;
		$panel_id.val(data.panel_id);
		$version.val(data.version);
	});

	$('input[type=radio][name=exist_normal]').change(function() {
		if (this.value === 'Y') {
			$bam_normal_location.prop('disabled', false);
			$bai_normal_location.prop('disabled', false);
			$bam_normal_location.prop('required', true);
			$bai_normal_location.prop('required', true);
		} else {
			$bam_normal_location.prop('disabled', true);
			$bai_normal_location.prop('disabled', true);
			$bam_normal_location.prop('required', false);
			$bai_normal_location.prop('required', false);
		}
	});

	$('#submitButton').click(function(e) {
		$registerForm.submit();
	});

	$registerForm.validate({
		debug: false,
		submitHandler: function(form) {
			var params = $(form).serialize();

			$.ajax({ method: 'POST', url: '/models/analysis', data: params })
				.done(function(msg) {
					console.log(msg);
					if (msg.success) {
						// noty('success', msg.message);
						$('#registered_analysis_id').text(msg.id)
						$('#messageModal').modal('show');

					} else {
						noty('danger', msg.message);
					}
				})
				.fail(function(err) {
					noty('danger', err);
				});
		},
		rules: {},
		messages: {}
	});
});
