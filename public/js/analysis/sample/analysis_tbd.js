$(document).ready(function() {
	$('#historyModal').on('show.bs.modal', function(event) {
		var obj = $(event.relatedTarget);
		// var id = $('#analysis_id').text();
		var data = obj.data('json');
		var title = obj.data('title');
		var type = obj.data('type');
		var tissue = obj.data('tissue');
		$('#historyModalLabel').text(title + ' History ').append($('<small>').text(data.analysis_id));
		var modal = $(this);
		var tbody = $('#historyTable tbody');
		tbody.empty();
		console.log(data);
		tbody.append($('<tr>')
			.append($('<td>').text(data.seq_no))
			.append($('<td>').text(data.status_short))
			.append($('<td>').text(data.modify_user))
			.append($('<td>').text(data.modify_date))
			.append($('<td>').text(data.message))
		);
	});
});
