$(function() {
	'use strict';
	var DRUG_URL = "https://www.drugbank.ca/unearth/q?searcher=drugs&approved=1&vet_approved=1&nutraceutical=1&illicit=1&withdrawn=1&investigational=1&query=";
	var MUTATION_URL = "http://cancer.sanger.ac.uk/cosmic/search?q=p.";
	var GENE_URL = "https://www.ncbi.nlm.nih.gov/gene?term=("; //+ 'EGFR[Sym]) AND 9606[Taxonomy ID]";
	var PUBMED_SUM_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi';
	var PUBMED_URL = "https://www.ncbi.nlm.nih.gov/pubmed/"; //+ 'EGFR[Sym]) AND 9606[Taxonomy ID]";
	var LEVEL_LINK = "<a target='oncokb' href='http://oncokb.org/#/levels'><i class='fa fa-external-link' aria-hidden='true'></i></a>";
	var CLOSE_LINK = "<a href='#' class='close' data-dismiss='alert'>&times;</a>";
	var NCI_URL = 'http://www.cancer.gov/about-cancer/treatment/drugs/';
	var DAILYMED_URL = 'http://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=';
	var GET_CLINVAR_ID_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=clinvar&retmode=json&term=';
	var GET_CLINICAL_SIGNIFICANCE_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=clinvar&retmode=json&id=';
	var CLINICAL_SIGNIFICANCE_URL = 'https://www.ncbi.nlm.nih.gov/clinvar/variation/';
	var SPINNER_HTML = '<span class="spinner hidden"><img src="/images/loading.gif" alt="Loading"/></span>';
	var SPINNER_LI_HTML = '<li class="spinner"><img src="/images/loading.gif" alt="Loading"/></li>';
	var LEVEL_TITLE = {
		'1': 'FDA-approved',
		'2A': 'Standard care',
		'2B': 'Standard care',
		'3A': 'Clinical evidence',
		'3B': 'Clinical evidence',
		'4': 'Biological evidence',
		'R1': 'Resistance'
	};
	var $table = $('#table');
	// Variant 화면에서 Classfication 부분 All 체크박스 이벤트.
	$('#classification_all').click(function() {
		// console.log(this.checked);
		// All 체크박스를 체크하면 아래 나머지 항목들은 모두 체크 해제가 된다.
		if (this.checked) {
			$('input[type=checkbox][name=classification]').prop('checked', false);
		}
	});
	// Classification 에서 하나 이상의 항목을 체크하였을 때, 
	// All 체크박스가 선택 되어있으면, 이를 해제하는 이벤트.
	$('input[type=checkbox][name=classification]').click(function() {
		// console.log(this.checked);
		if (this.checked && $('#classification_all').prop('checked')) {
			$('#classification_all').prop('checked', false);
		}
	});
	// Variant Update 버튼 이벤트.
	$('#filterButton').click(function(e) {
		// console.log($('#ex1').val());
		// console.log(getClassificationParameter());
		// console.log($('input[type=checkbox][name=cosmic]').prop('checked'));
		// 부트스트랩 테이블 모든 데이터를 지우고, 빈 데이터로 새로고침한다.
		$table.bootstrapTable('removeAll');
		$table.bootstrapTable('refresh', {});
		// 데이터를 받아온다.
		var tbData = $table.bootstrapTable('getData');
		// Table Data 가 존재 할 경우 Needle Plot 이 그려질 부분을 보여 준다.
		if (tbData.length > 1) {
			$('#analysisTab a:first').tab('show');
		} else {
			$('#analysisTab a:first').tab('show');
			// 어디 있는지 모르겠다. (#navi, #legend)
			$('#navi, #legend').css('visibility', 'hidden');
		}
	});
	/**
	 * Classification 항목에서 체크 된 값을 반환하는 함수.
	 */
	function getClassificationParameter() {
		// All 이 체크되어있을 경우 value="All" 을 반환.
		if ($('#classification_all').prop('checked')) {
			return $('#classification_all').val();
		}
		// All 이 아닌 다른 하나 이상의 항목이 체크 되었을 경우 배열에 담아 
		// 각각의 value 를 "," 로 문자열을 합쳐 반환한다.
		var checked = [];
		$('input[type=checkbox][name=classification]:checked').each(function(_i, _o) {
			checked.push(_o.value);
		});
		return checked.join(',');
	}
	/*
		사용 하지 않는 함수.
	*/
	function getDrugLink(value) {
		var array = value.split('|');
		var link = '';
		// console.log('value', value, 'array', array);
		if (array.length === 3) {
			var url;
			if (array[1] === 'N') {
				url = NCI_URL;
			} else {
				url = DAILYMED_URL;
			}
			link = '<a target=drug class=\'text-nowrap\' href=\'' + url + array[2] + '\'>' + array[0] + '</a>';
		} else {
			link = array[0];
		}
		// console.log(array, link);
		return link;
	}
	/**
	 * Gene 옆에 링크 태그를 추가하는 함수.
	 * 외부 링크 URL 와 Target 을 받아 문자열로 Anker 태그를 반환한다.
	 * @param {string} target "ncbi"
	 * @param {string} link "외부 링크 URL"
	 */
	function externalLink(target, link) {
		return ' <a title="' + target + '" target="' + target + '" href="' + link + '"><i class="fa fa-external-link" aria-hidden="true"></i></a> ';
	}
	/**
	 * Bootstrap Table 을 만드는 함수.
	 */
	$table.bootstrapTable({
		url: '/models/analysis/getAnalysisVariantList',
		classes: 'table',
		method: 'get',
		// cache: false, // False to disable caching of AJAX requests.
		width: '1200px',
		// showColumns: true,
		// showRefresh: true,
		// sortName: 'gene',
		// sortName: 'patientsOfPosition',
		// sortable: true,
		// sortOrder: 'desc',
		pagination: true,
		pageSize: 5,
		queryParams: function(params) {
			// cohort 변수는 analysis/cohort.js 에 정의되어있다. 
			// 해당 메소드의 값들은 localStorage(object) 에서 가져온다.
			params.source = cohort.getCohortSource();
			// Analysis_id & Cancer_type 은 숨겨진 input 태그로 존재하며 각각 value 값에 
			// Analysis_id 와 Cancer_type 값을 가지고 있다.
			params.analysis_id = $('#analysis_id').val();
			params.cancer_type = $('#cancer_type').val();
			params.frequency = $('#frequency').val();
			params.classification = getClassificationParameter();
			// In Cancer Genes - Cancer Gene Census 항목의 체크 여부를 파라미터로 전달한다.
			params.cosmic = $('input[type=checkbox][name=cosmic]').prop('checked') ?
				'Y' :
				'N';
			params.filter_option = cohort.getFilterOption();
			// params.driver = $('input[type=checkbox][name=driver]').prop('checked') ?
			// 	'Y' :
			// 	'N';
			// console.log('params:', params);
			return params;
		},
		// 첫 번째 행(row)을 활성화 하는 메소드.
		rowStyle: function(row, index) { // make first row active
			if (index === 0)
				return { classes: 'info' };
			return {};
		},
		columns: [{
			// 첫 번째 열(Column) 부터 순차적으로 되어있다.
			field: 'state',
			title: '#',
			radio: true,
			align: 'center',
			valign: 'middle',
			clickToSelect: true
		}, {
			field: 'gene',
			title: 'Gene',
			sortable: true,
			align: 'center',
			// 각 행(Row)의 값을 포맷해주는 메소드.
			formatter: function(value, row) {
				//var params = [row.cancer_type, row.analysis_id, row.gene, row.transcript];
				// return '<a target=\'ncbi\' href="http://www.ncbi.nlm.nih.gov/gene?term=(' + value + '[Sym]) AND 9606[Taxonomy ID]">' + value + '</a> ';
				return value + externalLink('ncbi', 'http://www.ncbi.nlm.nih.gov/gene?term=(' + value + '[Sym]) AND 9606[Taxonomy ID]');
			},
			events: 'tsEvents', // needleplot.js
		}, {
			field: 'class',
			title: 'Class',
			sortable: true,
			align: 'center',
			formatter: function(value, row) {
				return value.replace('_Mutation', '');
			}
		}, {
			field: 'vaf',
			title: 'VAF',
			halign: 'center',
			align: 'right',
			formatter: function(value, row) {
				if (value === undefined)
					return '';
				return (value * 100).toFixed(2) + '%';
			}
		}, {
			field: 'chr',
			title: 'Chr',
			align: 'center'
		}, {
			field: 'start_pos',
			title: 'Locus',
			halign: 'center',
			align: 'right',
			formatter: function(value, row) {
				var reg = /(^[+-]?\d+)(\d{3})/;
				var n = (value + '');

				while (reg.test(n))
					n = n.replace(reg, '$1' + ',' + '$2');
				return n;
			}
		}, {
			// formatter 메소드가 없는 경우에는 원본 데이터가 입력된다.
			field: 'cds',
			title: 'CDS Change',
			align: 'center',
			class: 'cds_change',
		}, {
			field: 'alt',
			title: 'AA Change',
			align: 'center'
		}, {
			field: 'uniprot_id',
			title: 'Protein',
			align: 'center',
			formatter: function(value, row) {
				//var params = [row.cancer_type, row.analysis_id, row.gene, row.transcript];
				// return '<a target=\'pfam\' href="http://pfam.xfam.org/protein/' + value + '">' + value + '</a> ';
				return value + externalLink('pfam', 'http://pfam.xfam.org/protein/' + value);
			},
			// events: 'tsEvents', // needleplot.js
		}, {
			field: 'pdomain',
			title: 'Domain',
			align: 'center',
			formatter: function(value, row) {
				if (value === undefined || value === '')
					return '';
				var domains = value.split(',');
				var domain_html = [];
				domains.forEach(function(domain) {
					var domain_info = domain.split('|');
					var info = ' <a href="#"><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="' + domain_info[1] + '"></i></a>';
					// var span = '<a target="pdomain" href="http://pfam.xfam.org/family/' + domain_info[0] + '">' + domain_info[0] + ' </a>';
					// return span + info;
					domain_html.push(domain_info[0] + info + externalLink('pdomain', 'http://pfam.xfam.org/family/' + domain_info[0]));

				});
				return domain_html.join(' ');
			}
		}, {
			field: 'dbsnp_rs',
			title: 'dbSNP',
			align: 'center',
			formatter: function(value, row) {
				if (value === null || value === '') return '';
				var html = '';
				value.split('|').forEach(function(rscode) {
					html += '<a target="dbSNP" title="dbSNP" href="https://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?searchType=adhoc_search&type=rs&rs=' + rscode + '">' + rscode + '</a>' +
						externalLink('ClinVar', 'https://www.ncbi.nlm.nih.gov/clinvar?term=' + rscode);
				});
				return html;
			}
		}, {
			field: 'dbsnp_rs',
			title: 'Clinical Significance(Clinvar)',
			align: 'center',
			formatter: function(value, row) {
				if (value === null || value === '') return '';
				var object_id = 'cs_' + Date.now();

				clinicalSignificanceInClinVar(value, object_id);
				return '<div><ul class="list-inline ul_nomargin"  id=' + object_id + '>' + SPINNER_LI_HTML + '</ul></div>';
			}
		}, {
			field: 'dbnsfp',
			title: 'dbNSFP',
			align: 'center',
			formatter: function(value, row) {
				if (value === 'T') return 'Benign';
				if (value === 'D') return 'Pathogenic';
				return '';
			}
		}, {
			field: 'patientsOfPosition',
			title: '<span>Frq. in Gene </span><a href="#"><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="no. of the patients who has a specific mutation among the patients with a specific gene in Frq. in Total"></i></a>',
			sortable: true,
			halign: 'center',
			align: 'right',
			formatter: function(value, row) {
				var pct = 0;
				if (row.patientsOfTranscript !== 0)
					pct = (row.patientsOfPosition / row.patientsOfTranscript) * 100;
				return pct.toFixed(2) + '%' + ' (' + row.patientsOfPosition + '/' + row.patientsOfTranscript + ')';
			}
		}, {
			field: 'patientsOfPosition',
			title: '<span>Frq. in Total </span><a href="#"><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="no. of the patients who has a specific mutation among the whole patients selected from public data (TCGA)"></i></a>',
			sortable: true,
			halign: 'center',
			align: 'right',
			formatter: function(value, row) {
				var pct = 0;
				// var patientsOfCancer = cohort.getFilteredCount();
				var patientsOfCancer = row.cntOfFilteredPatient;
				if (patientsOfCancer !== 0)
					pct = (value / patientsOfCancer) * 100;
				return pct.toFixed(2) + '%' + ' (' + value + '/' + patientsOfCancer + ')';
			}
		}, {
			field: 'fda_in_gene_drugs',
			title: 'Drugs',
			align: 'center',
			formatter: function(value, row) {
				var html = '';
				// var drugs_for_gene = (row.drugs_for_gene !== null) ? row.drugs_for_gene.split(',').length : 0;
				// var drugs_for_alt = (row.drugs_for_alt !== null) ? row.drugs_for_alt.split(',').length : 0;
				// Mining Drug & OncoKB Drug 의 개수를 구하는 함수를 호출하여 반환받고 변수에 입력시킨다.
				var miningdrugs = getMiningDrugCount(row.miningdrugs);
				var oncokbdrugs = getOncoKBDrugCount(row.oncokbdrugs);
				// drug 에 따라 각기 다른 CSS 적용.
				if (row.fda_in_gene_drugs.length > 0)
					html = html + '<span class="badge badge-agent-pink" data-toggle="tooltip" data-placement="top" title="Drugs for gene">' + row.fda_in_gene_drugs.length + "</span>";
				if (row.fda_in_alt_drugs.length > 0)
					html = html + '<span class="badge badge-agent-red" data-toggle="tooltip" data-placement="top" title="Drugs for gene/alteration">' + row.fda_in_alt_drugs.length + "</span>";
				if (oncokbdrugs > 0)
					html = html + '<span class="badge badge-primary" data-toggle="tooltip" data-placement="top" title="OncoKB drugs">' + oncokbdrugs + "</span>";
				if (miningdrugs > 0)
					html = html + '<span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Mining drugs">' + miningdrugs + "</span>";
				return html;
			}
		}]
	});

	var $drugstable = $('#drugstable');
	var $fda_in_gene_drugs = $('#fda_in_gene_drugs');
	var $fda_in_alt_drugs = $('#fda_in_alt_drugs');
	var $miningdrugs = $('#miningdrugs');
	var $oncokbdrugs = $('#oncokbdrugs');
	// 화면을 클릭하였을때, 팝업 창을 닫는 이벤트.
	$(document).on("click", ".popover .close", function() {
		$(this).parents(".popover").popover('hide');
	});
	// Drug 가 존재하면 Bootstrap Table 아래 테이블에 표시된다. 아래 이벤트는 표시된 
	// Drug 에 대한 클릭 이벤트이다.
	$miningdrugs.on('click', '.miningdrugs', function() {
		getMiningPubmedData($(this), $(this).data('content'));
	});

	$oncokbdrugs.on('click', '.oncokbdrugs', function() {
		getOncoKBPubmedData($(this), $(this).data('pmids'));
	});

	$('#fda_in_gene_drugs, #fda_in_alt_drugs').on('click', '.fda_approved_drug', function() {
		getFDAApprovedDrug($(this));
	});
	/**
	 * ClinVar API 를 호출하여 값과 링크를 결합한 문자열 태그를 Object_id(cs_ + "Data().now") 에 맞는 
	 * id 값을 찾아 append 를 해주는 함수.
	 * @param {string} dbsnp_rs RS ID
	 * @param {string} object_id Object ID
	 */
	function clinicalSignificanceInClinVar(dbsnp_rs, object_id) {
		dbsnp_rs.split('|').forEach(function(rscode) {
			$.ajax({ url: GET_CLINVAR_ID_URL + rscode }).done(function(data) {
				// console.log('success', data.esearchresult.idlist);
				data.esearchresult.idlist.forEach(function(clinvarId) {
					$.ajax({ url: GET_CLINICAL_SIGNIFICANCE_URL + clinvarId }).done(function(sig_data) {
						var desc = sig_data.result[clinvarId].clinical_significance.description;
						// console.log('success2', desc);
						if (desc !== 'Uncertain significance' && desc !== 'not provided')
							$('[id=' + object_id + ']').append('<li>' + desc + '(<a target="clinvar" href="' + CLINICAL_SIGNIFICANCE_URL + clinvarId + '">' + clinvarId + '</a>)</li>');
					}).error(function(request, status, error) {
						console.log(error);
						alert('Error!');
					}).always(function() {});

				});
			}).error(function(request, status, error) {
				console.log(error);
				alert('Error!');
			}).always(function() {
				var $spinner = $('[id=' + object_id + ']').children('.spinner');
				$spinner.remove();
			});
		});
	}
	/**
	 * Drug Table 을 그려주는 함수.
	 * @param {dom-object} row 
	 */
	function drawDrugs(row) {
		drawFdaApprovedDrugs(row.fda_in_gene_drugs, $fda_in_gene_drugs, 'FDA approved drugs for gene');
		drawFdaApprovedDrugs(row.fda_in_alt_drugs, $fda_in_alt_drugs, 'FDA approved drugs for alteration');
		// drawCurationDrugs(row.drugs_for_alt, $drugs_for_alt);
		drawOncokbdrugs(row.oncokbdrugs);
		drawMiningdrugs(row.miningdrugs);
	}
	/**
	 * Mining Drug 배열을 받아 값이 없을 경우 0, 있을 경우
	 * 중복되는 값을 배제한 개수를 반환하는 함수.
	 * @param {array} value Mining Drug arrya or none
	 */
	function getMiningDrugCount(value) {
		var count = 0;
		if (!Array.isArray(value) || value.length === 0)
			return count;

		var prev_drug_name = '';
		value.forEach(function(mining) {
			var name = mining.drug;
			if (name !== prev_drug_name) count++;

			prev_drug_name = name;
		});
		return count;
	}
	/**
	 * Drug Table 에서 mining drug 항목을 그려주는 함수.
	 * @param {array} value mining drug list 
	 */
	function drawMiningdrugs(value) {
		if (!Array.isArray(value) || value.length === 0)
			return $miningdrugs.html('');

		var html = [];
		var drugs = _.groupBy(value, 'drug');
		Object.keys(drugs).forEach(function(drug) {
			var data_content = JSON.stringify(drugs[drug]);
			var data = '<a class="miningdrugs" tabindex="0" role="button" data-placement="bottom" data-trigger="manual" data-toggle="popover" data-html="true" data-viewport="#maincontent" title="Sentence - ' + drug + CLOSE_LINK + '"  data-content=\'' + data_content + '\'>' + drug + SPINNER_HTML + '</a>';
			html.push(data);
		});

		$miningdrugs.html(html.join('<span>, </span>'));
	}
	/**
	 * OncoKB Drug 배열을 받아 값이 없을 경우 0 을 있을 경우
	 * 각 문자열 원소를 "," 로 나눠 그 배열의 개수를 누적합산 하여 반환하는 함수.
	 * @param {array} value OncoKB Drug Array or none
	 */
	function getOncoKBDrugCount(value) {
		var count = 0;
		if (!Array.isArray(value) || value.length === 0)
			return count;

		value.forEach(function(oncokb) {
			count += oncokb.drugs.split(',').length;
		});
		return count;
	}
	/**
	 * Drug 와 Drug URL 과 링크 값을 받아서 anker 태그로 이루어진 url list 
	 * 를 만들어 반환하는 함수.
	 * @param {string} drug 
	 * @param {string} drugs_url 
	 * @param {string} multilink 
	 */
	function getDrugLinkNew(drug, drugs_url, multilink) {
		// console.log('getDrugLinkNew', drug, drugs_url, multilink)
		if (!drugs_url) return drug;
		var url = [];
		if (multilink) {

			drug.split('+').forEach(function(item) {
				var link = drugs_url.shift();
				if (link && link != 'NULL') {
					link = link.replace('%NCI%', NCI_URL).replace('%DMED%', DAILYMED_URL);
					url.push('<a target=\'drug\' href=\'' + link + '\' >' + item + '</a>');
				} else {
					url.push(item);
				}
			});
		} else {
			var link = drugs_url.shift();
			if (link && link != 'NULL') {
				link = link.replace('%NCI%', NCI_URL).replace('%DMED%', DAILYMED_URL);
				url.push('<a target=\'drug\' href=\'' + link + '\' >' + drug + '</a>');
			} else {
				url.push(drug);
			}
		}

		console.log(url);

		return url.join(' + ');
	};
	/**
	 * Drug Table 의 FDA Approved gene & alteration 항목을 그려주는 함수.
	 * @param {array} drugs 
	 * @param {dom-object} $target 
	 * @param {string} type 
	 */
	function drawFdaApprovedDrugs(drugs, $target, type) {
		if (!Array.isArray(drugs) || drugs.length === 0)
			return $target.html('');

		var html = [];
		drugs.forEach(function(drug) {
			// var data = '<a data-toggle="modal" data-target="#drugModal" data-type="' + type + '" data-drug=\'' + JSON.stringify(drug) + '\'>' + drug.agent + '</a>';
			var data = '<a class="fda_approved_drug" tabindex="0" role="button" data-placement="bottom" data-trigger="focus" data-toggle="popover" data-html="true" data-viewport="#maincontent"  data-type="' + type + '" data-drug=\'' + JSON.stringify(drug) + '\'>' + drug.agent + SPINNER_HTML + '</a>';
			html.push(data);
		});

		$target.html(html.join('<span>, </span>'));
	}
	/**
	 * Drug 테이블의 OncoKB Drug 를 그려주는 함수.
	 * @param {array} value 
	 */
	function drawOncokbdrugs(value) {
		// console.log('oncokbdrugs', value);
		if (!Array.isArray(value) || value.length === 0)
			return $oncokbdrugs.html('');

		var html = [];
		value.forEach(function(oncokb) {
			// console.log('drug', oncokb);
			var title = "Level " + oncokb.level + " - " + LEVEL_TITLE[oncokb.level] + ', ' + oncokb.drugs + CLOSE_LINK;;
			var content = oncokb.cancer_type_full + oncokb.pmid_for_drug + oncokb.level;
			var data = '<a class="oncokbdrugs oncokb_level_' + oncokb.level + '" tabindex="0" role="button" data-placement="bottom" data-trigger="manual" data-toggle="popover" data-html="true" data-viewport="#maincontent" title="' + title + '" data-pmids="' + oncokb.pmid_for_drug + '">Level ' + oncokb.level + ' (' + oncokb.drugs + ')' + SPINNER_HTML + '</a>';
			html.push(data);
		});
		$oncokbdrugs.html(html.join('<span>, </span>'));
	}
	/**
	 * FDA Approved 된 Drug 들을 외부 API 를 통해서 값을 받아와
	 * 그 내용을 파싱해서 정리한다음 팝업창으로 보여주는 함수.
	 * @param {dom-object} obj 
	 */
	function getFDAApprovedDrug(obj) {
		var $genelink = obj;
		var $spinner = $genelink.children('span');
		$spinner.removeClass('hidden');

		var drug = $genelink.data('drug');
		var type = $genelink.data('type');
		var type_html = '<span>' + type + ': </span>';
		var drug_url = getDrugLinkNew(drug.agent, (drug.link) ? drug.link.split(',') : [], drug.multi_link);
		$genelink.attr('data-title', type_html + drug_url + CLOSE_LINK);
		// console.log(drug, '\n', type);
		// console.log(drug_url, drug.pmid)

		var options = {
			db: 'pubmed',
			retmode: 'json',
			id: drug.pmids
		};

		var refs = {};
		if (drug.ref_pmids) {
			drug.ref_pmids.split(',').forEach(function(ref) {
				var item = ref.split('|');
				var name = item[0];
				var pmid = item[1];

				if (!refs[name]) refs[name] = { pmids: [] };
				if (pmid && pmid.trim() !== '') refs[name].pmids.push(pmid);
			});
		}
		var content = '';
		$.ajax({ url: PUBMED_SUM_URL, data: options }).done(function(data) {
			// console.log('success', data);
			Object.keys(refs).forEach(function(key) {
				content += '<div class="panel panel-default narrow-panel"><div class="panel-heading narrow-head">' + key + '</div>';
				content += '<ul class="list-group">';
				refs[key].pmids.sort(function(a, b) { return b - a }).forEach(function(pmid) {
					// console.log(data.result[pmid], '\n', key)
					content += '<li class="list-group-item"><p>' + data.result[pmid].title + '</p>' +
						'<var>' + data.result[pmid].sortfirstauthor + ' et al. ' + data.result[pmid].source +
						', ' + data.result[pmid].pubdate +
						'</var>' + '<span style="float:right;">PMID:<a target="pubmed" href="' + PUBMED_URL + pmid + '">' +
						pmid + '</a></span>' + '</li>';
				});
				content += '</ul>'
				content += '</div>'
			});
		}).error(function(request, status, error) {
			alert('Error!');
		}).always(function() {
			$genelink.attr('data-content', content);
			$spinner.addClass('hidden');
			$genelink.popover({ html: true, trigger: 'manual' });
			$genelink.popover('show');
		});
	}
	/**
	 * 태그와 컨텐츠를 받아 OncoKB API 에서 전달 받은 데이터를
	 * parsing 해서 parent tag 에 data-content 로 값을 넣고
	 * popup 창을 띄워 내용을 보여주는 함수.
	 * @param {dom-object} td parent tag object
	 * @param {string} pmids PubMed Mining Drug Content data 
	 */
	function getOncoKBPubmedData(obj, pmids) {
		var data = {
			db: 'pubmed',
			retmode: 'json',
			id: pmids
		};

		var content = '<ul class="list-unstyled">';
		var $spinner = obj.children('span');

		$spinner.removeClass('hidden');
		$.ajax({ url: PUBMED_SUM_URL, data: data }).done(function(data) {
			// console.log('success', data);
			if (data.result) {
				data.result.uids.sort(function(a, b) { return b - a }).forEach(function(name) {
					// console.log(data.result[name].title, data.result[name].sortfirstauthor, data.result[name].sortpubdate, data.result[name].source);
					content = content + '<li><p>' + data.result[name].title + '</p>' + '<var>' + data.result[name].sortfirstauthor + ' et al. ' + data.result[name].source + ', ' + data.result[name].pubdate + '</var>' + '<span style="float:right;">PMID:<a target="pubmed" href="' + PUBMED_URL + name + '">' + name + '</a></span>' + '</li>'; // , data.result[name].sortfirstauthor, data.result[name].sortpubdate, data.result[name].source);
				});
			}
			obj.attr('data-content', content + '</ul>');
			obj.popover('show');
		}).error(function(request, status, error) {
			alert('Error!');
		}).always(function() {
			// isLoading = false;
			$spinner.addClass('hidden');
		});
	}
	/**
	 * 태그와 컨텐츠를 받아 PubMed API 에서 전달 받은 데이터를
	 * parsing 해서 parent tag 에 data-content 로 값을 넣고
	 * popup 창을 띄워 내용을 보여주는 함수.
	 * drug table 을 그려주는 contents 들을 모두 getVariantList 로 요청을 해서 데이터를 db 에 받아온다.
	 * @param {dom-object} td parent tag object
	 * @param {string} contents PubMed Mining Drug Content data 
	 */
	function getMiningPubmedData(obj, contents) {
		console.log(contents)
		var pmids = contents.map(function(cotent) {
			return cotent.pmid;
		}).join(',');

		console.log(pmids);

		var data = {
			db: 'pubmed',
			retmode: 'json',
			id: pmids
		};

		// console.log(data);

		var content = '<ul class="list-unstyled">';
		var $spinner = obj.children('span');

		$spinner.removeClass('hidden');
		// 고대에서 받은 데이터를 처리하는 부분이다.
		// DB 에서 받은 Sentence 데이터에서 Gene, Drug, Mutation_inText 와 일치하는 부분을
		// HTML Link 로 변경한다. 
		console.log(PUBMED_SUM_URL, data);
		$.ajax({ url: PUBMED_SUM_URL, data: data }).done(function(data) {
			console.log(data);
			// console.log('success', data);
			if (data.result) {
				data.result.uids.sort(function(a, b) { return b - a }).forEach(function(name) {
					var mining = _.find(contents, function(content) {
						return content.pmid === name;
					});
					// 고대에서 받은 데이터가 센텐스가 아니면 다른 이름으로 가져오고
					// 고대에서 HTML 에 클래스 (색상정보 등) 을 추가한 데이터가 오면 아래 정규식은 지우고 센텐스를 그냥 반환한다.
					// 고대에서 받은 데이터가 클래스 정보가 없을 경우에는 아래의 코드를 그냥 둔다.
					var regexp = new RegExp(mining.drugintext, "gi");
					var sentence = mining.sentence.replace(new RegExp(mining.drugintext, "g"), '<a target=drug class=text-success href=\'' + DRUG_URL + mining.drug + '\'>' + mining.drugintext + ' </a>');
					sentence = sentence.replace(new RegExp(mining.mutationintext, "g"), '<a target=mutatation class=text-danger href=\'' + MUTATION_URL + mining.mutationintext + '\'>' + mining.mutationintext + ' </a>');
					sentence = sentence.replace(new RegExp(mining.geneintext, "g"), '<a target=gene class=text-warning href=\'' + GENE_URL + mining.gene + '[Sym]) AND 9606[Taxonomy ID]\'>' + mining.geneintext + ' </a>');

					content = content + '<li><p>' + sentence + '</p><var>' + data.result[name].sortfirstauthor + ' et al. ' + data.result[name].source + ', ' + data.result[name].pubdate + '</var>' + '<span style="float:right;">PMID:<a target="pubmed" href="' + PUBMED_URL + name + '">' + name + '</a></span>' + '</li>'; // , data.result[name].sortfirstauthor, data.result[name].sortpubdate, data.result[name].source);
				});
			}
			obj.attr('data-content', content + '</ul>');
			obj.popover('show');
		}).error(function(request, status, error) {
			alert('Error!');
		}).always(function() {
			// isLoading = false;
			$spinner.addClass('hidden');
		});
	}
	// Bootstrap Table 이 로드가 완료되었을때 이벤트.
	$table.on('load-success.bs.table', function(_event, _data, _args) {
		// 데이터가 존재하지 않을 경우 Needle Plot 을 그려주지 않는다.
		if (_data === undefined || _data.length === 0) {
			// Remove previous chart...
			$("div[id^=needleplot]").css("display", "none");
			return;
		}

		$("div[id^=needleplot]").css("display", "block");
		var data = _data[0];

		// main.js에 있는 부분을 여기 써주는 이유는, 비동기로 데이터를 읽어오기 때문에, main.js 가 실행된 후에 table row 데이터가 들어오기 때문이다.
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();

		$table.bootstrapTable('check', 0); //first row check,
		$drugstable.removeClass('hidden');
	});
	// 페이지별 5개의 항목을 보여주는 데 페이지 이동시 가장 첫번째 항목이 체크되게 하는 이벤트.
	$table.on('page-change.bs.table', function(_table, page, cnt) {
		// console.log("Current page number: ", number, ", Count row on a page: ", cnt);
		$table.bootstrapTable('check', ((page - 1) * cnt));
	});
	// Bootstrap Table 에서 Row 를 선택하였을 때 발생하는 이벤트.
	$table.on('check.bs.table', function(_table, _row) {
		drawDrugs(_row);
		$('.selected').addClass('info').siblings().removeClass('info');
		// Needle Plot 을 그려주는 코드.
		var bcr = document.querySelector('#genemutationplot').getBoundingClientRect();
		var width = bcr.width;

		document.querySelector('#main').innerHTML = '';
		document.querySelector('.tab-content')
			.style.height = '401px';

		$.ajax({
			type: 'GET',
			url: '/rest/needleplot',
			data: {
				'source': cohort.getCohortSource(),
				'cancer_type': _row.cancer_type,
				'analysis_id': _row.analysis_id,
				'gene': _row.gene,
				'transcript': _row.transcript,
				'classification': getClassificationParameter(),
				'filter': cohort.getFilterOption()
			},
			beforeSend: function() {
				bio.loading().start(document.querySelector('#main'), Math.ceil(width) - 20, 400);
			},
			success: function(d) {
				bio.variants({
					element: '#main',
					width: Math.ceil(width) - 20,
					height: 400,
					data: {
						variants: d.data,
						type: _row.cancer_type.toUpperCase(),
					}
				});

				bio.loading().end();
			},
		});
		// IGV Plot 을 그려주는 함수.
		showIgv();
	});
	// Bootstrap Table Popover event 함수.
	$table.on('post-body.bs.table', function(_event, _data, _args) {
		$('[data-toggle="popover"]').popover();
	});
	/**
	 * IGV Plot 을 그려주는 함수.
	 */
	var showIgv = function() {
		var data = $table.bootstrapTable('getData');
		// $('.nav-tabs .active').text() 를 변수로 지정하지 않은 이유는, 변수로 지정할 경우
		// 아래 tab 이벤트 부분에서 이전에 등록될 때 가져온 변수를 계속 사용하기 때문이다.
		if ($('.nav-tabs .active').text() === 'Read Alignment Plot') {
			// igv.js 에서 view 함수를 호출하여 탭이 바뀌었을 때 IGV Plot 을 그려준다.
			Igv.view('tab', getIndex(), data);
		} else {
			// Needle Plot Tab 을 클릭하였을 때 Needle Plot 을 그려준다.
			$('a[data-toggle="tab"]').off().on('shown.bs.tab', function(e) {
				if ($('.nav-tabs .active').text() !== 'Gene Mutation Plot') {
					Igv.view('tab', getIndex(), data);
				} else {
					var d = data[getIndex()];
					var bcr = document.querySelector('#genemutationplot').getBoundingClientRect();
					var width = bcr.width;

					document.querySelector('#main').innerHTML = '';
					document.querySelector('.tab-content')
						.style.height = '401px';

					$.ajax({
						type: 'GET',
						url: '/rest/needleplot',
						data: {
							'source': cohort.getCohortSource(),
							'cancer_type': d.cancer_type,
							'analysis_id': d.analysis_id,
							'gene': d.gene,
							'transcript': d.transcript,
							'classification': getClassificationParameter(),
							'filter': cohort.getFilterOption()
						},
						beforeSend: function() {
							bio.loading().start(document.querySelector('#main'), Math.ceil(width) - 20, 400);
						},
						success: function(dd) {
							bio.variants({
								element: '#main',
								width: Math.ceil(width) - 20,
								height: 400,
								data: {
									variants: dd.data,
									type: d.cancer_type.toUpperCase(),
								}
							});

							bio.loading().end();
						},
					});
				}
			});
		}
	};
	/**
	 * Bootstrap Table 에서 현재 선택된 항목의 Index 를 가져와 반환하는 함수.
	 */
	var getIndex = function() {
		var result = 0;

		$('input[name="btSelectItem"]:checked').each(function(index, ele) {
			result = $(ele).data().index;
		});

		return result;
	};

	// window.tsEvents = {
	//     'click #gene': function(_event, _value, _row, _index) {
	//         $(this).closest('tr').addClass('info').siblings().removeClass('info');
	//         Init.requireJs(
	//             "analysis_needle",
	//             "/rest/needleplot?cancer_type=" + _row.cancer_type + "&analysis_id=" + _row.analysis_id + "&gene=" + _row.gene + "&transcript=" + _row.transcript + "&classification=" + getClassificationParameter() + "&filter=" + cohort.getFilterOption().join(',')
	//         );
	//     }
	// };
});
