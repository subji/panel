$(function() {

	var $table = $('#table');
	$table.bootstrapTable({
		url: '/models/panel/list',
		classes: 'table',
		method: 'get',
		// search: true,
		// showRefresh: true,
		showColumns: false,
		pagination: true,
		// toolbar: "#toolbar",
		toolbarAlign: 'right',
		pageSize: 5,
		columns: [{
			field: 'panel_id',
			title: 'Panel ID',
			sortable: true,
			valign: 'middle',
		}, {
			field: 'name',
			title: 'Name',
			valign: 'middle',
		}, {
			field: 'version',
			title: 'Version',
			align: 'center',
			valign: 'middle',
		}, {
			field: 'hugo_symbols',
			title: 'Gene list',
			// align: 'center',
			valign: 'middle',
			width: '50%',
			formatter: function(value, row) {
				if (!value) return '';
				// var array = value.split(',');
				// var html = '<select class="hugo_symbols" name="hugo_symbols[]" multiple="multiple" disabled>';
				// for (var i in array) {
				// 	html += ' <option value="' + array[i] + '" selected>' + array[i] + '</option>';
				// }
				// html += '</select>';
				// return html;
				var array = value.split(',');
				var html = '<div style="white-space:normal;">';
				for (var i in array) {
					html += '<span class="label label-primary">' + array[i] + '</span> ';
				}
				html += '</div>';
				return html;
			}
		}, {
			field: 'hugo_symbols',
			title: '# of Gene',
			align: 'center',
			valign: 'middle',
			formatter: function(value, row) {
				if (!value) return 0;
				var array = value.split(',');
				return array.length;
			}
			// }, {
			// 	field: 'comment',
			// 	title: 'Comment',
			// 	class: 'comment',
			// 	formatter: function(value, row) {
			// 		return '<span title="' + value + '">' + value + '</span>';
			// 	}
			// }, {
			// 	field: 'create_at',
			// 	title: 'Create at',
			// 	sortable: true,
			// 	align: 'center',
			// 	formatter: function(value, row, index) {
			// 		var date = new Date(value);
			// 		return moment(date).fromNow();
			// 	}
		}, {
			title: 'Manage',
			field: 'id',
			valign: 'middle',
			formatter: function(value, row) {
				return '<a href="#" data-toggle="modal" data-target="#detailModal" data-op="update" data-row=\'' + JSON.stringify(row) + '\'><i class="fa fa-pencil-square-o action-link"/><span>edit</span></a>' +
					'<br><a href="#" data-toggle="modal" data-target="#detailModal" data-op="versionup" data-row=\'' + JSON.stringify(row) + '\'><i class="fa fa-arrow-up action-link"/><span>version up</span></a>' +
					'<br><a href="#" class="delete" data-id="' + row.id + '"><i class="fa fa-trash-o action-link"/><span>delete</span></a>';
			}
		}]
	});

	$table.on('load-success.bs.table', function(_event, _data, _args) {
		$('.tagsinput').tagsinput("refresh");
		// $('.hugo_symbols').select2({
		// 	width: '100%'
		// });
	});

	var $op = $('#op');
	var $id = $('#id');
	var $panel_id = $('#panel_id');
	var $name = $('#name');
	var $version = $('#version');
	var $hugo_symbols = $('#hugo_symbols');
	// $hugo_symbols.tagsinput({
	// 	tagClass: 'label label-primary'
	// });
	// $hugo_symbols.tagsinput("refresh");
	var beforeItemAdd = function(event) {
		var obj = $(this);
		if (event.item !== event.item.toUpperCase()) { // 소문자인 경우 대문자로 변환
			event.cancel = true;
			obj.tagsinput('add', event.item.toUpperCase());
		} else { //대문자인경우
			console.log('toUpperCase', event);
			$.ajax({ method: 'GET', url: '/models/gene/exist', data: { gene: event.item } })
				.done(function(data) {
					console.log('exist', data);
					if (!data.exist) {
						alert(event.item + ' does not exist.')
						// event.cancel = true; // not work. too late
						obj.tagsinput('remove', event.item);
					}
				})
				.fail(function(err) {});
		}
	};

	$('#detailModal').on('show.bs.modal', function(event) {
		var link = $(event.relatedTarget);
		$op.val(link.data('op'));
		var row = link.data('row');

		console.log('row', row);

		if ($op.val() === 'update' || $op.val() === 'versionup') { //update or version up
			$id.val(row.id);
			$panel_id.val(row.panel_id);
			$name.val(row.name);

			$version.val(row.version);
			if ($op.val() === 'versionup') $version.val(row.version + 1);

			$hugo_symbols.val(row.hugo_symbols);
		}

		$hugo_symbols.tagsinput({
			tagClass: 'label label-primary',
			onTagExists: function(item, $tag) {
				alert('Already exist.')
				$tag.hide().fadeIn();
			}
		});
		$hugo_symbols.on('beforeItemAdd', beforeItemAdd);

	});

	$('#detailModal').on('hidden.bs.modal', function(e) {
		console.log('hidden');
		$hugo_symbols.tagsinput('removeAll');
		$hugo_symbols.unbind('beforeItemAdd');
		$hugo_symbols.tagsinput('destroy');
		//clear form
		$('#panelForm')[0].reset();
		// clear previous error
		$("#panelForm").validate().resetForm(); //TODO  Not Working
		$('#panelForm input').removeClass('error');
		// $table.bootstrapTable('refresh');
	});

	var noty = function(type, message) {
		console.log(type, message);
		$.notify({
			icon: 'glyphicon glyphicon-info-sign',
			message: message,
		}, {
			type: type,
			mouse_over: 'pause',
			placement: {
				from: "top",
				align: "center"
			},
			z_index: 1051,
			delay: 500,
			timer: 1000,
		});
	};

	$('#saveButton').click(function(e) {
		$("#panelForm").submit();
	});

	$("#panelForm").validate({
		debug: false,
		submitHandler: function(form) {
			var params = $(form).serialize();
			var method = ($op.val() === 'insert' || $op.val() === 'versionup') ? 'POST' : 'PUT';

			$.ajax({ method: method, url: '/models/panel', data: params })
				.done(function(msg) {
					console.log(msg);
					if (msg.success) {
						noty('success', msg.message);
						$('#detailModal').modal('hide')
						$table.bootstrapTable('refresh');
					} else {
						noty('danger', msg.message);
					}
				})
				.fail(function(err) {
					noty('danger', err);
				});
		},
		rules: {},
		messages: {}
	});

	$table.on('click', 'a.delete', function(e) {
		bootbox.confirm('삭제하시겠습니까?', function(result) {
			if (result) {
				var id = $(e.target).data('id');
				console.log('delete', id);
				var json = { id: id };
				$.ajax({ method: "DELETE", url: '/models/panel', data: json })
					.done(function(msg) {
						if (msg.success) {
							noty('success', msg.message);
							$table.bootstrapTable('refresh');
						} else {
							noty('danger', msg.message);
						}
					})
					.fail(function(err) {
						noty('danger', err);
					});
			}
		});
	});
});
