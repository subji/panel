'use strict';

(function ()	{
	var sidebar = document.querySelector('#sidebar'),
			navbar = document.querySelector('.navbar'),
			container = document.querySelector('.container');

	var sbcr = sidebar.getBoundingClientRect(),
			nbcr = navbar.getBoundingClientRect();

	container.style.width = '1625px';
	container.style.height = '855px';
	container.style.margin = '0px';
	container.style.visibility = 'visible';
	container.style.overflow = 'hidden';

	document.querySelector('#maincontent').style.marginBottom = '0px';

	var width = parseFloat(container.style.width),
			height = parseFloat(container.style.height);

	$.ajax({
	type: 'GET',
	url: landscapeURL,
	data: landscapeReqParams,
	beforeSend: function ()	{
		bio.loading().start(document.querySelector('#main'), 1620, 850);
	},
	success: function (d)	{
		bio.landscape({
			element: '#main',
			width: 1620,
			height: 600,
			data: {
				pq: 'p',
				type: landscapeReqParams.cancer_type.toUpperCase(),
				data: d.data,
				title:d.data.name,
			},
		})
		
		bio.loading().end();
	},
});
}());